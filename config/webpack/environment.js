const { environment } = require('@rails/webpacker')

// KHS: parte de bootstrap y sus dependencias
const webpack = require('webpack')
environment.plugins.prepend('Provide',
   new webpack.ProvidePlugin({
      $: 'jquery/src/jquery',
      jQuery: 'jquery/src/jquery',
      Popper: ['popper.js', 'default']
   })
)
// Fin

module.exports = environment
