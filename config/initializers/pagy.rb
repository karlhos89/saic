#encoding: UTF-8
# KHS: Initializer para Pagy
# Pagy gem dependency
require 'pagy/extras/bootstrap'

# Default :empty_page (other option :last_page or :exception)

# last_page
require 'pagy/extras/overflow'
Pagy::VARS[:overflow] = :last_page

# exception
# require 'pagy/countless'
# require 'pagy/extras/overflow'
# Pagy::VARS[:overflow] = :exception
