Rails.application.routes.draw do
   # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
   # KHS: agregar rutas url => controlador#acción que responde
   # Rutas Devise
   devise_for :users, :controllers => {:registrations => "registrations", :sessions => "sessions"}
   
   # Obtener información
   # get
   # Insertar información
   # post
   # Actualizar información
   # patch
   # Eliminar información
   # delete
   
   # HOME
   root to: "home#index" # Ruta base
   get "/home", to: "home#index"
   # Regresar a la página anterior
   get "/return_back/:default", to: "application#return_back", as:"return_back_or_default"
   
   # Readings (Text)
   get "readings", to: "readings#index" # Filtro: todas
   get "readings/read", to: "readings#index_read" # Filtro: leídas
   get "readings/unread", to: "readings#index_unread" # Filtro: nuevas (no leídas)
   get "readings/recommended", to: "readings#index_recommended" # Filtro: recomendadas
   get "readings/shared", to: "readings#index_shared" # Filtro: compartidas
   get "readings/uploaded", to: "readings#index_uploaded" # Filtro: subidas (por el usuario)
   # Reading: new/create
   get "readings/new", to: "readings#new"
   post "/texts", to: "readings#create"
   # Reading: show/read
   get "readings/:id", to: "readings#show", as: "readings_show"
   # Reading: edit/update
   get "readings/:id/edit", to: "readings#edit", as: "readings_edit"
   patch "readings/:id", to: "readings#update", as: :text
   # Reading: delete
   delete "readings/:id/delete", to: "readings#destroy", as: "readings_destroy"
      
   # Flashcards (UserWord, InteractedWord)
   get "flashcards", to: "flashcards#index" # Filtro: todas
   get "flashcards/next", to: "flashcards#index_next" # Filtro: siguiente repaso a tiempo
   get "flashcards/expired", to: "flashcards#index_expired" # Filtro: repaso caducado
   get "flashcards/standby", to: "flashcards#index_standby" # Filtro: repaso en espera
   get "flashcards/challenge", to: "flashcards#index_challenge" # Filtro: repaso de desafío
   # Flashcard: show/read
   get "flashcards/review/:filter/:size", to: "flashcards#review", as: "flashcards_review" # Repaso/Estudio de Tarjetas de Memoria
   
   # ¿En qué orden se va a listar?
   # ¿Una lista unificada marcada por conjuntos y que cada cambio de status se salga de show?
   # UserWord: show/read
   
   # Vocabulary / Word / Internal Services JS
   post "vocabulary/add_word_from_reading", to: "vocabulary#add_word_from_reading"
   post "vocabulary/update_word_from_flashcard", to: "vocabulary#update_word_from_flashcard" # Actualización de Tarjeta de Memoria
   post "vocabulary/add_words_from_test", to: "vocabulary#add_words_from_test" # Agregar palabras desde Test inicial
   # Vocabulary / Word / Services (Google Cloud Platform)
   post "vocabulary/word/translate", to: "vocabulary#translate_service"
   post "vocabulary/word/images", to: "vocabulary#images_service"
   post "vocabulary/word/text_to_speech", to: "vocabulary#text_to_speech_service"
   
   
   
   
   
   # Redirección de rutas
   get "/users", to: redirect("/")
   
   # Rutas de error
   # get '/foo', :to => redirect('/foo.html')
   get "*a", to: "errors#file_not_found"
   get "422", to: "errors#unprocessable"
   get "500", to: "errors#internal_server_error"
end
