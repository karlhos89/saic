#encoding: UTF-8

class SRInteractedWord
   # InteractedWord (only used data)
   attr_accessor :id
   attr_accessor :repetitions
	attr_accessor :encounters
	attr_accessor :record_repetition
	attr_accessor :user_first_review
	attr_accessor :user_last_review
	attr_accessor :user_next_review
   attr_accessor :probability
   
   
   ## Public: inicializa el modelo de datos
	# 
	# id                :: Identificador (integer)
	# repetitions       :: Número de repeticiones correctas y continuas (integer)
	# encounters        :: Número de encuentros con la palabra (integer)
	# record            :: Número de repasos correctos, no consecutivos (integer)
	# user_first_review :: Fecha y hora de la 1a revisión de la palabra (String) (AAAA-MM-DD ZZ)
	# user_last_review  :: Fecha y hora de la última revisión de la palabra (String)
   # user_next_review  :: Fecha y hora de la siguiente revisión de la palabra (String)
   # probability       :: Probabilidad con la que se calcula user_next_review (default: 0.5)
   def initialize(id, repetitions, encounters, record_repetition, user_first_review, user_last_review, user_next_review, probability)
      @id                = id
      @repetitions       = repetitions
		@encounters        = encounters
		@record_repetition = record_repetition
		@user_first_review = user_first_review
		@user_last_review  = user_last_review
		@user_next_review  = user_next_review
		@probability       = probability
   end
end