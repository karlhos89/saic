#encoding: UTF-8

require_relative 'models/sr_interacted_word'
require_relative 'SR'

class SpacedRepetition
   
   ## Public: constructor
   def initialize()
      
   end
   
   
   ## Public: calcula la fecha y hora del siguiente repaso/revisión de una palabra
   #
   # interacted_word  :: Datos de la InteractedWord (TRInteractedWord.rb)
   # favorite_hour    :: Hora favorita del estudiante/usuario para estudiar (integer)
   # review_note      :: Calificación/nota de la revisión (correct_review|wrong_review)
   # @return SRInteractedWord. En caso de error, devuelve nil
   def calculate_next_review(interacted_word, favorite_hour, review_note)
      # Si el repaso fue correcto: repetition++. De lo contrario, repetition=1
		interacted_word.repetitions = (review_note==SR::CORRECT_REVIEW) ? interacted_word.repetitions += 1 : interacted_word.repetitions = 1
		puts "Repetitions:::: #{interacted_word.repetitions}" if (SR::VERBOSE)
		
		# Si el repaso fue correcto o no: encounters++
		interacted_word.encounters += 1
		puts "Encounters::::: #{interacted_word.encounters}" if (SR::VERBOSE)
		
		# Si el repaso fue correcto: record++, sino record queda con el mismo valor
		interacted_word.record_repetition = (review_note==SR::CORRECT_REVIEW) ? interacted_word.record_repetition += 1 : interacted_word.record_repetition
		puts "Record::::::::: #{interacted_word.record_repetition}" if (SR::VERBOSE)
      
      # Repetición espaciada normal o ajustada
      if (favorite_hour == -1) # normal
         puts 'normal (sin horario favorito)' if (SR::VERBOSE)
         # Ecuación: t = -s (ln p), determina el tiempo entre el repaso actual y el siguiente
         # t: tiempo entre el repaso actual y el siguiente
         # s: número de repasos/repeticiones
         # p: probabilidad de mínimo 50% de recordar la información
         time = ( - interacted_word.repetitions * Math.log(SR::PROBABILITY) ).round(SR::DECIMALS)
         puts "Intervalo t:::: #{time} días" if (SR::VERBOSE)
         interacted_word.probability = SR::PROBABILITY
         puts "Probabilidad::: #{SR::PROBABILITY}" if (SR::VERBOSE)
      else # ajustada
         puts 'ajustada (con horario favorito)' if (SR::VERBOSE)
         # Ecuación: t = -s (ln p), determina el tiempo entre el repaso actual y el siguiente
         # t: tiempo entre el repaso actual y el siguiente
         # s: número de repasos/repeticiones
         # p: probabilidad de mínimo 50% de recordar la información
         time = ( - interacted_word.repetitions * Math.log(SR::PROBABILITY) ).to_i
         # Filtrar primera repetición: time == 0
         time = (time == 0) ? 1 : time
         puts "Intervalo t:::: #{time} días" if (SR::VERBOSE)
         interacted_word.probability = Math.exp(- time.to_f / interacted_word.repetitions.to_f)
         puts "Probabilidad::: #{interacted_word.probability} *" if (SR::VERBOSE)
      end
		
      # Cálculo del siguiente repaso
      last_review = DateTime.now() # El ultimo repaso es HOY, a menos que se desee cambiar este parametro por el de entrada
      # last_review = DateTime.strptime(interacted_word.user_last_review, '%Y-%m-%d %H:%M:%S %z')
      next_review = last_review + time
      
		puts "UserLastReview: #{last_review.strftime('%Y-%m-%d %H:%M:%S %z')}" if (SR::VERBOSE)
		puts "UserNextReview: #{next_review.strftime('%Y-%m-%d %H:%M:%S %z')}" if (SR::VERBOSE)
      
      interacted_word.user_last_review = last_review.strftime('%Y-%m-%d %H:%M:%S %z')
      interacted_word.user_next_review = next_review.strftime('%Y-%m-%d %H:%M:%S %z')
      
		return interacted_word
   end
end