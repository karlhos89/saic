#encoding: UTF-8

module SR
   # Parámetros generales
   VERBOSE      = false # Indica si la ejecución generará log o no
   
   # Parámetros de revisión/repaso
   CORRECT_REVIEW = 'correct_review' unless (const_defined?(:CORRECT_REVIEW))
   WRONG_REVIEW   = 'wrong_review' unless (const_defined?(:WRONG_REVIEW))
   
   # Parámetros de cálculo
   DEFAULT_HOUR = -1 unless (const_defined?(:DEFAULT_HOUR))
   # Probabilidad mínima del algoritmo SR
   PROBABILITY  = 0.5 unless (const_defined?(:PROBABILITY))
   # Cantidad de decimales con los que trabajará el algorimoTR
   DECIMALS     = 15 unless (const_defined?(:DECIMALS))
end