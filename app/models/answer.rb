class Answer < ApplicationRecord
   # Answer - AssociatedAnswers - Questions
   has_many :associated_answers
   has_many :questions, through: :associated_answers
   # Answer - AnsweredQuestions - Users
   has_many :answered_questions
   has_many :users, through: :answered_questions
   # Answer - AnsweredQuestions - Questions
   has_many :questions, through: :answered_questions
end
