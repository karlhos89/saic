# General Constants
module Const
   # User
   TEACHER_ROLE      = "teacher" unless (const_defined?(:TEACHER_ROLE))
   STUDENT_ROLE      = "student" unless (const_defined?(:STUDENT_ROLE))
   DEFAULT_CODE      = "SAIC" unless (const_defined?(:DEFAULT_CODE))
   EXPERIMENTAL_CODE = "SAIC-exp" unless (const_defined?(:EXPERIMENTAL_CODE))
   CONTROL_CODE = "SAIC-ctrl" unless (const_defined?(:CONTROL_CODE))
   # Text|AssociatedText
   UNREAD_STATUS   = 'unread' unless (const_defined?(:UNREAD_STATUS))
   READ_STATUS     = 'read' unless (const_defined?(:READ_STATUS))
   DEFAULT_QUALITY = 0.0 unless (const_defined?(:DEFAULT_QUALITY)) # Recommendation quality
   # Readings filters
   ALL_READINGS_FILTER         = 'all' unless (const_defined?(:ALL_READINGS_FILTER))
   UNREAD_READINGS_FILTER      = UNREAD_STATUS unless (const_defined?(:UNREAD_READINGS_FILTER))
   READ_READINGS_FILTER        = READ_STATUS unless (const_defined?(:READ_READINGS_FILTER))
   RECOMMENDED_READINGS_FILTER = 'recommended' unless (const_defined?(:RECOMMENDED_READINGS_FILTER))
   SHARED_READINGS_FILTER      = 'shared' unless (const_defined?(:SHARED_READINGS_FILTER))
   UPLOADED_READINGS_FILTER    = 'uploaded' unless (const_defined?(:UPLOADED_READINGS_FILTER))
   # UserWord|InteractedWord (no en base de datos)
   EXPIRED_STATUS = 'expired' unless (const_defined?(:EXPIRED_STATUS))
   NEXT_STATUS    = 'next' unless (const_defined?(:NEXT_STATUS))
   STANDBY_STATUS = 'standby' unless (const_defined?(:STANDBY_STATUS))
   DAYS_RANGE     = 1.0 unless(const_defined?(:DAYS_RANGE))
   # Flashcards difficulty
   LOW_DIFFICULTY   = 'easy' unless (const_defined?(:LOW_DIFFICULTY))
   MEDIUM_DIFFICULTY = 'medium' unless (const_defined?(:MEDIUM_DIFFICULTY))
   HIGH_DIFFICULTY   = 'hard' unless (const_defined?(:HIGH_DIFFICULTY))
   # Flashcards filters
   ALL_FLASHCARDS_FILTER     = 'all' unless (const_defined?(:ALL_FLASHCARDS_FILTER))
   EXPIRED_FLASHCARDS_FILTER = EXPIRED_STATUS unless (const_defined?(:EXPIRED_FLASHCARDS_FILTER))
   NEXT_FLASHCARDS_FILTER    = NEXT_STATUS unless (const_defined?(:NEXT_FLASHCARDS_FILTER))
   STANDBY_FLASHCARDS_FILTER = STANDBY_STATUS unless (const_defined?(:STANDBY_FLASHCARDS_FILTER))
   CHALLENGE_FLASHCARDS_FILTER  = 'challenge' unless (const_defined?(:CHALLENGE_FLASHCARDS_FILTER))
   # Lemmatize
   LEMMATIZE_TEXT = "lemmatize_text" unless (const_defined?(:LEMMATIZE_TEXT))
   LEMMATIZE_WORD = "lemmatize_word" unless (const_defined?(:LEMMATIZE_WORD))
   # Otros
   EMAIL_ADMIN  = "mailto:carlos.peralta18ca@cenidet.edu.mx" unless (const_defined?(:EMAIL_ADMIN))
   # BROWSER
   FIREFOX_BROWSER = 'firefox' unless (const_defined?(:FIREFOX_BROWSER))
   EDGE_BROWSER    = 'edge' unless (const_defined?(:EDGE_BROWSER))
   SAFARI_BROWSER  = 'safari' unless (const_defined?(:SAFARI_BROWSER))
   OTHER_BROWSER   = 'other' unless (const_defined?(:OTHER_BROWSER))
end



class ApplicationRecord < ActiveRecord::Base
   self.abstract_class = true

   # Acceso a los elementos de Vocabulary (Text / AssociatedWord / TextWord)
   Expression = Struct.new(:word, :lemma, :word_frequency, :lemma_frequency)
   
   
   
   protected
   
   # LEMMATIZE
   
   # Lematizar un texto o una palabra (content) mediante Freeling Service
   def self.lemmatize(type, content)
      not_valid_chars = ['?', '_'] # Caracteres que devuelve freeling en tokens especiales
      expressions = Array.new()
      
      begin
         # Consulta remota a freeling por servidor local
         analyzer = FreeLing::Analyzer.new(content, :server_host => 'localhost:50005')
      

         # Obtener palabras y lemas del ánalisis de freeling (y tokenizar)
         tokens = analyzer.tokens.map { |token|
            # tokens va obteniendo el último valor manejado (en lugar de return)
            # Ignorar NP (noun|proper)
            (token.tag == 'NP') ? nil : Expression.new(token.form, token.lemma, 0, 0)
         }
         
         # Corregir lemmas de freeling de cada token
         tokens.compact!
         tokens.each do |token|
            
            # Ignorar token cuando no es de una palabra
            unless ( is_number(token.word) )
               
               # Reemplazar lemas que son números y que deberían ser nombres de números/grados
               if (is_number(token.lemma))
                  # puts "REPLACE: #{token.lemma} WITH #{token.word}"
                  token.lemma = token.word
               else # Reemplazar lemas que contienen tokens especiales
                  not_valid_chars.each do |char|
                     if (token.word.include?(char) || token.lemma.include?(char)) # Sin reemplazo
                        # puts "REPLACE: #{token.lemma} WITH #{token.word}"
                        token.lemma = token.word
                     end
                  end
               end
               
               token.word.gsub!("_","-")
               token.lemma.gsub!("_","-")
               
               # Comprobar valores e insertar
               if (token.word != nil && token.word != nil)
                  token.word.downcase!()
                  token.lemma.downcase!()
                  
                  # puts "token: #{token.word}/#{token.lemma}"
                  expressions.insert(-1, token)
               end
               
            end
         end
      rescue StandardError => exception
         puts "ERROR LOG: Freeling service ERROR (#{type})"
         puts "LOG: #{exception}"
         return nil
      else
         puts "LOG: Freeling service OK (#{type})"
      end
      
      if (type == Const::LEMMATIZE_TEXT)
         return expressions # Devuelve un Array<Expression>
      elsif (type == Const::LEMMATIZE_WORD)
         lemma = ""
         if (expressions.size() > 0)
            lemma = expressions.first.lemma
         else
            puts "LOG: Sin datos en arreglo 'expressions' (#{type})"
         end
         
         return lemma # Devuelve el lema de la palabra
      else
         puts "LOG: Opción inválida en lemmatize (#{type})"
      end
   end
   
   # Comprobar si un string es un número o una cadena con letras
   def self.is_number(string)
      return string.to_f.to_s == string.to_s || string.to_i.to_s == string.to_s
   end
end
