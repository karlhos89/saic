class AssociatedGender < ApplicationRecord
   validates :user_id, uniqueness:true
   
   # AssociatedGender - User
   belongs_to :user
   # AssociatedGender - Gender
   belongs_to :gender
end
