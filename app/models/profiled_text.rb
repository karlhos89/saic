class ProfiledText < ApplicationRecord
   validates :text_id, uniqueness:true
   
   # ProfiledText - Text
   belongs_to :text
   # ProfiledText - CEFRProfile
   belongs_to :cefr_profile
end
