class AssociatedQuestion < ApplicationRecord
   validates :question_id, uniqueness:true
   
   # AssociatedQuestion - Text
   belongs_to :text
   # AssociatedQuestion - Question
   belongs_to :question
end
