class ProfiledUser < ApplicationRecord
   validates :user_id, uniqueness:true
   
   # ProfiledUser - User
   belongs_to :user
   # ProfiledUser - CEFRProfile
   belongs_to :cefr_profile
end
