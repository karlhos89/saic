class LearningStyle < ApplicationRecord
   validates :name, uniqueness:true
   
   # LearningStyle - AssociatedLearningStyles - Users
   has_many :associated_learning_styles
   has_many :users, through: :associated_learning_styles
end
