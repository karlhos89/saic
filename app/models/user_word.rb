require 'google/cloud/translate' # GoogleCloudPlatform (Translate)
require 'rest-client' # GoogleCloudSearch
require 'json' # GoogleCloudSearch
require 'google/cloud/text_to_speech' # GoogleCloudPlatform (TextToSpeech)
 
class UserWord < ApplicationRecord
   validates :word, uniqueness:true
   
   # UserWord - InteractedWords - Users
   has_many :interacted_words
   has_many :users, through: :interacted_words
   #UserWord - InteractedWords - Texts
   has_many :texts, through: :interacted_words
   
   
   # ATRIBUTOS FORÁNEOS
   attr_accessor :word_es
   attr_accessor :sentence
   attr_accessor :word_image
   attr_accessor :repetitions
   attr_accessor :encounters
   attr_accessor :record_repetition
   attr_accessor :user_first_review
   attr_accessor :user_last_review
   attr_accessor :user_next_review
   attr_accessor :probability
   
   # InteractedWord
   
   def delete_interacted_word(user)
      InteractedWord.where(user:user).where(user_word:self).destroy_all()
   end
   
   # Google Cloud Services
   
   def self.translate(word)
      project_id    = "saic-1589757036380" # ID de proyeto (Google Cloud Platform)
      language_code = "es" # Idioma objetivo

      # Instanciar cliente del API
      client = Google::Cloud::Translate.new(version: :v2, project_id: project_id)

      # Invocar servicio al cliente
      translation = client.translate(word, to: language_code)
      # Texto::::::::::: word
      # Traducción:::::: translation
      # Idioma fuente::: translation.from
      # Idioma objetivo: translation.to
      
      return translation
   end
   
   def self.images(word)
      puts "Search images:"
      file           = File.read("#{Rails.root}/config/google_custom_search/SAIC-s20200724.json")
      credentials    = JSON.parse(file) # Convertir string a JSON
      cx             = credentials["customSearchID"] # ID de Custom Search Engine
      key            = credentials["consoleKey"] # Key de Google Cloud Console
      search_type    = 'image' # Tipo de búsqueda
      number_results = '10' # Cantidad de resultados
      # word           = 'mexico' # Palabra de búsqueda de imágenes
      
      begin
         puts 'Request: builting...'
         request = RestClient::Request.new(
            method: :get,
            url: "https://customsearch.googleapis.com/customsearch/v1?cx=#{cx}&key=#{key}&searchType=#{search_type}&num=#{number_results}&q=#{word}",
            verify_ssl: true
         )
         
         puts 'Request: executing...'
         response = request.execute()
         
         puts 'Request: done!'
         result = JSON.parse(response.to_s)
         
         # puts "Result: #{result}"
      rescue StandardError => exception
         puts "Request: exception (#{exception})"
      else
         puts 'Request complete.'
      end
      
      items = result["items"]
      # puts "Items: #{items}"
      
      # String: image_link<LINKS_SEPARATOR>thumbnail_link<IMAGE_SEPARATOR>... (repite patrón)
      @images = ""
      
      items.each do |item|
         @images = @images.concat(item["link"].to_s) # Link de la imagen
         @images = @images.concat("<LINK_SEPARATOR>")
         
         image_data = item["image"]
         @images = @images.concat(image_data["thumbnailLink"].to_s) # Link de la thumbnail
         @images = @images.concat("<IMAGE_SEPARATOR>")
      end
      
      # puts "IMAGES: " + @images
      
      return @images
   end
   
   def self.text_to_speech(word)
      # Instanciar cliente del API
      client = Google::Cloud::TextToSpeech.text_to_speech()

      # Texto de entrada
      synthesis_input = { text: word }

      # Construir la petición de voz:
      # Código de lenguaje ("en-US")
      # Generador de voz ssml ("neutral")
      voice = {
         language_code: "en-US",
         ssml_gender: "NEUTRAL"
      }

      # Tipo de audio que se requiere de vuelta
      audio_config = { audio_encoding: "MP3" }

      # Realizar la petición/solicitud TextoAVoz(TextToSpeech):
      # Entrada de texto
      # Parámetros de voz
      # Tipo de archivo de audio
      response = client.synthesize_speech(
      input:        synthesis_input,
      voice:        voice,
      audio_config: audio_config
      )
      
      # Audio MP3 en binario
      return response.audio_content # MP3 en formato binario
   end
   
end
