class Question < ApplicationRecord
   # Question - AssociatedQuestions - Texts
   has_many :associated_questions
   has_many :texts, through: :associated_questions
   # Question - AssociatedAnswers - Answers
   has_many :associated_answers
   has_many :answers, through: :associated_answers
   # Question - AnsweredQuestions - Users
   has_many :answered_questions
   has_many :users, through: :answered_questions
   # Question - AnsweredQuestions - Answers
   has_many :answers, through: :answered_questions
end
