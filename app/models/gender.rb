class Gender < ApplicationRecord
   validates :name, uniqueness:true
   
   # Gender - AssociatedGenders - Users
   has_many :associated_genders
   has_many :users, through: :associated_genders
end
