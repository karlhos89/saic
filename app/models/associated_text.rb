class AssociatedText < ApplicationRecord
   validates_uniqueness_of :user_id, :scope => :text_id
   
   # AssociatedText - User
   belongs_to :user
   # AssociatedText - Text
   belongs_to :text   
end
