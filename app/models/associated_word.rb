class AssociatedWord < ApplicationRecord   
   validates_uniqueness_of :text_id, :scope => :text_word_id
   
   
   # AssociatedWord - Text
   belongs_to :text
   # AssociatedWord - TextWord
   belongs_to :text_word
end
