class User < ApplicationRecord
   devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable
   
   # Validaciones
   #validates_presence_of :first_name, :last_name, :birthdate, :gender_id, :learning_style_id, :cefr_profile_id
   validates :first_name, presence: true
   validates :last_name, presence: true
   validates :birthdate, presence: true
   validates :gender_id, presence: true, on: :create
   validates :learning_style_id, presence: true, on: :create
   validates :cefr_profile_id, presence: true, on: :create
   validates :first_name, format: {with: /\A[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]*\z/} # Letras, acentos y espacio
   validates :last_name, format: {with: /\A[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]*\z/}
   validates :birthdate, format: {with: /\A[0-9]{4}+(\/|-)[0-9]{2}+(\/|-)[0-9]{2}\z/}   
   validates :email, uniqueness: true
   validates :password, format: {with: /\A[a-zA-Z0-9]*\z/} # Sólo letras y números
   validates :code_confirmation, allow_blank: true, format: {with: /\A([0-9]{4}+(-)[0-9]{2}+(-)[0-9]{4}+(-)[0-9]{2}|Student|student|STUDENT)\z/}, on: :update
   
   # User - GroupedUsers - Groups
   has_many :grouped_users
   has_many :groups, through: :grouped_users
   # User - InteractedWords - UserWords
   has_many :interacted_words
   has_many :user_words, through: :interacted_words
   # User - InteractedWords - Texts
   has_many :texts, through: :interacted_words
   # User - AssociatedLearningStyles - LearningStyles
   has_many :associated_learning_styles
   has_many :learning_styles, through: :associated_learning_styles
   # User - AssociatedGender - Gender
   has_many :associated_genders
   has_many :genders, through: :associated_genders
   # User - ProfiledUsers - CEFRProfiles
   has_many :profiled_users
   has_many :cefr_profiles, through: :profiled_users
   # User - AssociatedTexts - Texts
   has_many :associated_texts
   has_many :texts, through: :associated_texts
   # User - AnsweredQuestions - Questions
   has_many :answered_questions
   has_many :questions, through: :answered_questions
   # User - AnsweredQuestions - Answers
   has_many :answers, through: :answered_questions
   
   
   # ATRIBUTOS PROPIOS
   
   def update_role(role) # User.role
      if (self.valid?)
         self.update(role:role)
         self.update(code:get_code()) if (role == Const::STUDENT_ROLE)
      end
   end
   
   def get_code() # Determina el codigo de acceso: código de docente, código de estudiante (default, experimental o control)
      # DESACTIVAR ASIGNACIÓN
      #return Const::EXPERIMENTAL_CODE
      #return Const::DEFAULT_CODE
      
      # GRUPO EXPERIMENTAL (Sólo por tiempo limitado)
      
      # today_datetime   = DateTime.now() 
      final_datetime   = DateTime.strptime('2021-01-01 00:00:10-06', '%Y-%m-%d %H:%M:%S %z')
      # difference = (final_datetime - today_datetime).to_f
      
      # return Const::EXPERIMENTAL_CODE if (difference >= 0)
      
      # GRUPOS ASIGNADOS
      
      datetime = final_datetime.strftime('%Y-%m-%d %H:%M:%S %z')
      experimental_users = User.where("created_at > '#{datetime}'").where(code: Const::EXPERIMENTAL_CODE).size()
      control_users      = User.where("created_at > '#{datetime}'").where(code: Const::CONTROL_CODE).size()
      
      # puts "EXP: #{experimental_users}"
      # puts "CON: #{control_users}"
      
      if (experimental_users <= control_users)
         return Const::EXPERIMENTAL_CODE
      else
         return Const::CONTROL_CODE
      end
   end
   
   
   # ATRIBUTOS FORÁNEOS
   
   attr_accessor :gender_id
   attr_accessor :learning_style_id
   attr_accessor :cefr_profile_id
   attr_accessor :code_confirmation # Atributo auxiliar/temporal
   
   
   # AssociatedGender
   
   def insert_gender_association()
      AssociatedGender.create(user:self, gender_id:self.gender_id) if (self.valid?)
   end
   
   def update_gender_association()
      AssociatedGender.update(user:self, gender_id:self.gender_id) if (self.valid?)
   end
   
   def delete_gender_association()
      AssociatedGender.find_by(user:self).destroy()
   end
   
   # AssociatedLearningStyle
   
   def insert_learning_style_association()
      AssociatedLearningStyle.create(user:self, learning_style_id:self.learning_style_id) if (self.valid?)
   end
   
   def update_learning_style_association()
      AssociatedLearningStyle.update(user:self, learning_style_id:self.learning_style_id) if (self.valid?)
   end
   
   def delete_learning_style_association()
      AssociatedLearningStyle.find_by(user:self).destroy()
   end
   
   # ProfiledUser (for CefrProfile)
   
   def insert_cefr_association()
      ProfiledUser.create(user:self, cefr_profile_id:self.cefr_profile_id) if (self.valid?)
   end
   
   def update_cefr_association()
      # Tener cuidado con las comparaciones entre instancias de base de datos (se recomienda convertir a string, int, float, etc)
      old_profile = ProfiledUser.find_by(user_id:self.id).cefr_profile_id.to_i
      new_profile = self.cefr_profile_id.to_i
      
      # Si se cambia el perfil MCER, se deben actualizar las lecturas
      if (old_profile != new_profile)
         # Perfil guardado en BD: ProfiledUser.find_by(user_id: self.id).cefr_profile_id
         # Nuevo perfil: self.cefr_profile_id
         # Delete AssociatedText, excepto owner (subidos) y read (leídos)
         self.texts.each do |text|
            puts "UserID: #{self.id}"
            puts "TextID: #{text.id}"
            skip_instruction = AssociatedText.find_by(user:self, text:text).owner || AssociatedText.find_by(user:self, text:text).status.eql?(Const::READ_STATUS)
            
            text.delete_text_association(self) unless (skip_instruction)
         end
         
         ProfiledUser.update(user:self, cefr_profile_id:self.cefr_profile_id) if (self.valid?)
      end
   end
   
   def delete_cefr_association()
      ProfiledUser.find_by(user:self).destroy()
   end
   
   # Delete All User Data
   
   def destroy_all_data()
      # Delete foreign attributes: AssociatedGender, AssocitedLerningStyle, ProfiledUser
      delete_gender_association()
      delete_learning_style_association()
      delete_cefr_association()
      
      # Delete AssociatedText
      self.texts.each do |text|
         text.delete_text_association(self)
      end
      
      # Delete AssociatedText
      self.user_words.each do |user_word|
         user_word.delete_interacted_word(self)
      end
   end
   
end
