require "freeling-analyzer"

class Text < ApplicationRecord
   # Validaciones
   validates_presence_of :title, :body, :cefr_profile_id
   validates :title, format: { with: /\A[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\s!?,.;:'"()\-]*\z/ } # --
   validates :body, format: { with: /\A[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\s!?,.;:'"()\-]*\z/ }
   
   # Text - InteractedWords - UserWords
   has_many :interacted_words
   has_many :user_words, through: :interacted_words
   # Text - InteractedWords - Users
   has_many :users, through: :interacted_words
   # Text - ProfiledTexts - CEFRProfiles
   has_many :profiled_texts
   has_many :cefr_profiles, through: :profiled_texts
   # Text - AssociatedTexts - Users
   has_many :associated_texts
   has_many :users, through: :associated_texts
   # Text - AssociatedWords - TextWords
   has_many :associated_words
   has_many :text_words, through: :associated_words
   # Text - AssociatedQuestions - Questions
   has_many :associated_questions
   has_many :questions, through: :associated_questions
   
   
   # ATRIBUTOS FORÁNEOS
   attr_accessor :cefr_profile_id
   
   
   # ProfiledUser (for CefrProfile)
   
   def insert_cefr_association()
      ProfiledText.create(text:self, cefr_profile_id:self.cefr_profile_id)
   end
   
   def update_cefr_association()
      if (ProfiledText.where(text:self).any?)
         ProfiledText.where(text:self).update(cefr_profile_id:self.cefr_profile_id)
      end
   end
   
   def delete_cefr_association()
      ProfiledText.find_by(text:self).destroy()
   end
   
   # AssociatedText (for User)
   
   def insert_text_association(user, owner)
      unless (AssociatedText.where(text:self, user:user).any?)
         AssociatedText.create(text:self, user:user, owner:owner, status:Const::UNREAD_STATUS, recommendation:false, recommendation_quality:Const::DEFAULT_QUALITY)
      end
   end
   
   def update_status(user, status)
      AssociatedText.where(user:user).find_by(text:self).update(status:status)
   end
   
   def delete_text_association(user)
      AssociatedText.where(user:user).find_by(text:self).destroy()
   end
   
   def delete_all_text_associations()
      AssociatedText.where(text:self).destroy_all()
   end
   
   # AssociatedWord (for TextWord)
   
   def insert_word_association(text_word, top_word, word_frequency, lemma_frequency)
      unless (AssociatedWord.where(text:self, text_word:text_word).any?)
         AssociatedWord.create(text:self, text_word:text_word, top_word:top_word, word_frequency:word_frequency, lemma_frequency:lemma_frequency)
      end
   end
   
   def delete_all_word_associations()
      AssociatedWord.where(text:self).destroy_all()
   end
   
   # InteractedWord (for User and UserWord)
   
   def delete_all_interacted_word_associations()
      InteractedWord.where(text:self).update_all("text_id = null")
   end
   
   
   
   ### Procesamiento de Lenguaje Natural (PLN)
   
   # Tokenizar el cuerpo del texto de manera especial, se utiliza para el reader de readings/show
   # Token: palabra, palabra con apóstrofo simple, número(s) y signo de puntuación
   def tokenize()
      text = self.body
      tokens = Array.new()
      element = ""
      
      text = text.gsub(/\n/, " # ") # El símbolo de almohadilla reemplazará al NEW LINE
      textArray = text.split(' ')
      
      textArray.each do |substring|
         element = ""
         
         substring.each_char { |char|
            
            if (element.length == 0) # Si element está vacío, concatenar char
               element.concat(char)
               
            else # En otro caso...
               
               # Si char es una letra (o apóstrofo simple)
               if (char.match(/[a-zA-Z]/) || char.match(/[']/))
                  # Si el último caracter de element es una letra (o apóstrofo simple)
                  if (element[-1, 1].match(/[a-zA-Z]/)  || element[-1, 1].match(/[']/))
                     element.concat(char)
                  else                        
                     # puts element
                     tokens << element
                     element = ""
                     element.concat(char)
                  end
               
               # Si char es un número
               elsif (char.match(/[0-9]/))
                  # Si el último caracter de element es un número
                  if (element[-1, 1].match(/[0-9]/))
                     element.concat(char)
                  else
                     # puts element
                     tokens << element
                     element = ""
                     element.concat(char)
                  end
                  
               # En otro caso... (char no es letra (o apóstrofo simple) ni número)
               else
                  # puts element
                  tokens << element
                  element = ""
                  element.concat(char)
               end
               
            end
         }
         
         if (element.length > 0)
            # puts element
            tokens << element
            element = ""
         end
         
      end
      
      # Corrección de comillas simples iniciales y finales en una palabra
      tokens_temp = Array.new(tokens)
      tokens.clear()
      initial_quote = false
      final_quote = false
      
      tokens_temp.each do |token|
         initial_quote = false
         final_quote = false
         
         # Detectar y remover comillas simples iniciales
         if (token[0, 1].match(/'/)) # [posición, número de caracteres]
            token[0] = " "
            token.strip!
            initial_quote = true
         end
         
         # Detectar y remover comillas simples finales
         if (token[-1, 1].match(/'/)) # [posición, número de caracteres]
            token[-1] = " "
            token.strip!
            final_quote = true
         end
         
         tokens << "'" if (initial_quote)
         tokens << token
         tokens << "'" if (final_quote)
      end
      
      # tokens.each do |token|
      #    puts token
      # end
      return tokens
   end
   
   # Contruir una oración utilizando el cuerpo del texto, una palabra y su posición en dicho texto.
   def build_sentence(word_position)
      sentence = ""
      i = 0 # Contador
      brackets = Array.new()
      space = true
      
      self.tokenize().each do |token|
         if (!token.match(/'+/) && token.match(/\A[a-zA-Z']*\z/)) # Palabra con/sin apóstrofo
            sentence.concat(" ") if (space)
            sentence.concat(token)
            space = true
         elsif (token.match(/\A[0-9]*\z/)) # Número
            sentence.concat(token)
         elsif (token.match(/#/))
            sentence.concat("")
         else # Cualquier otro caracter
            if (token=='"' || token=='\'' || token=='(' || token=='-')
               if (brackets.include?(token))
                  space = true
                  brackets.delete(token)
               else
                  space = false
                  sentence.concat(" ") if(brackets.length==0)
                  brackets << token
               end
            end
            
            sentence.concat(token)
            
            if (token=='.' && i>word_position)
               break
            elsif (token=='.' && i<word_position)
               sentence = ""
            end
         end
         
         i+=1
      end
      
      return sentence
   end
   
   # Preprocesamiento del texto antes de insertar a base de datos
   
   # Crear un vocabulary array < Expresion(word, lemma, word_frequency, lemma_frequency)
   def self.create_vocabulary(text_body)
      text = String.new(text_body)
      vocabulary = Array.new()
      
      normalized_text = normalize(text)
      expressions = lemmatize(Const::LEMMATIZE_TEXT, normalized_text)
      
      return nil if (expressions.nil? || expressions.size==0)
      
      # Obtener frecuencia de palabra y vocabulario por palabra única
      expressions.each do |exp|
         existed_exp = false
         
         # Si existe la palabra se suma a la frecuencia +1
         vocabulary.each do |element|
            if (element.word == exp.word)
               element.word_frequency += 1
               existed_exp = true
               # puts "Frecuencia: #{element.word_frequency} #{element.word}"
               break
            end
         end
         
         # Si no existe la palabra, entonces se agrega al array
         unless (existed_exp)
            exp.word_frequency = 1
            vocabulary.insert(-1, exp)
            # puts "Inserta: #{exp.word}"
         end
         
      end
      
      # Obtener frecuencia de lemma
      vocabulary.each do |element|
         lemma_frequency = 0
         vocabulary_copy = Array.new(vocabulary)
         
         # Calcular frecuencia sólo de los lemas faltantes
         if (element.lemma_frequency == 0)
            vocabulary_copy.each do |aux|
               if (aux.lemma == element.lemma)
                  lemma_frequency += aux.word_frequency
               end
            end
            
            element.lemma_frequency = lemma_frequency
         end
         
         # puts "#{element.word_frequency} :: #{element.lemma_frequency} (#{element.word}/#{element.lemma})"
      end
      
      return vocabulary
   end
   
   # Obtener el número de palabras del texto corto
   def self.count_tokens(text_body)
      text = String.new(text_body)
      
      normal_text = normalize(text)
      tokens = normal_text.split(' ')
      # En los textos originales existen contracciones,
      # en el preprocesamiento se normaliza y reemplazan las contracciones por palabras completas
      
      return tokens.size
   end

   # Obtener el número de palabras distintas en el texto (types)
   def self.count_types(vocabulary)
      return vocabulary.size
   end

   # Obtener el número de lemas distintos en el texto (lemmas)
   def self.count_lemmas(vocabulary)
      lemmas = Array.new()
      
      # Obtener lemas (sin repetir)
      vocabulary.each do |element|
         existed_lemma = false
         
         # Comprobar si el lema ya existe en el arreglo
         lemmas.each do |lemma|
            if (lemma == element.lemma)
               existed_lemma = true
               break
            end
         end
         
         # Si el lema no existe, entonces insertar
         unless (existed_lemma)
            lemmas.insert(-1, element.lemma)
         end
         
      end
      
      return lemmas.size
   end
   
   
   protected
   
   
   # Normalizar texto
   def self.normalize(text)
      text.delete!('.') # Remover signos de puntuación
      text.delete!(',') # Optimiza: text = text.delete(',')
      text.delete!(':')
      text.delete!(';')
      text.delete!('?')
      text.delete!('!')
      text.delete!('$')
      text.delete!('(')
      text.delete!(')')
      text.delete!('"')
      # text.downcase!() # Se omite para que freeling detecté los NP (noun|proper)
      text = replace_contractions(text) # Reemplazar contracciones
      text.gsub!("'","") # Reemplazar comilla simple de enfásis
      text.gsub!("-"," ") # Reemplazar guiones
      text.gsub!(" -"," ")
      text.gsub!("- "," ")

      # puts text
      return text
   end
   
   # Reemplazar contracciones por palabras completas
   def self.replace_contractions(text)
      text.gsub!("can't","can not")
      text.gsub!("won't","will not")
      text.gsub!("'m"," am")
      text.gsub!("'s"," is")
      text.gsub!("s'","s is")
      text.gsub!("'re"," are")
      text.gsub!("'d"," had")
      text.gsub!("'ve"," have")
      text.gsub!("'ll"," will")
      text.gsub!("n't"," not")
      text.gsub!("'n"," and")

      return text
   end
   
   # Modificar comillas simples
   def self.modify_simple_quotes(text)
      text.strip!
      text.gsub!("'","''")

      return text
   end
   
end
