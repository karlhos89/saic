class TextWord < ApplicationRecord
   validates :word, uniqueness:true
   
   # TextWord - AssociatedWords - Texts
   has_many :associated_words
   has_many :texts, through: :associated_words
   
   
      # Para UPDATES ver los métodos de Text.rb para AssociatedText

   
   
end
