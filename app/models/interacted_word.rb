class InteractedWord < ApplicationRecord
   # validates_uniqueness_of :user_id, :scope => [:user_word_id, :word_es]
   
   # InteractedWord - User
   belongs_to :user
   # InteractedWord - Text
   belongs_to :text, optional: true # Cuando el campo es foreign key de otra tabla, pero puede ser null
   # InteractedWord - UserWord
   belongs_to :user_word
   
   
   def self.datetime_to_text(datetime)
      # Fecha de DateTime a String
      return datetime.strftime('%Y-%m-%d %H:%M:%S %z')
   end
   
   def self.days_to_text(days_f)      
      days_abs = days_f.abs() # Obtener valor absoluto
      days = days_abs.floor() # Obtener valor entero sin decimales
      hours = ((days_abs.to_f - days.to_f) * 24).floor() # Obtener decimales
      
      if (days > 0)
         days_text = days == 1 ? "#{days} día" : "#{days} días"
         hours_text = hours == 1 ? "#{hours} hora" : "#{hours} horas"
         text = "#{days_text} y #{hours_text}"
      else
         text = hours == 1 ? "#{hours} hora" : "#{hours} horas"
      end
      
      return text
   end
   
   # Días hasta el próximo repaso de la InteractedWord (Flashcard)
   def days_until_next_review()
      # Fecha de String a DateTime
      today = DateTime.now()
      next_review = DateTime.strptime(self.user_next_review.to_s, '%Y-%m-%d %H:%M:%S %z').new_offset(today.zone)
      
      days = next_review.to_f - today.to_f # Resultado en segundos
      days = days/60 # Segundos
      days = days/60 # Minutos
      days = days/24 # Horas
      
      # puts "Next Review: #{next_review}"
      # puts "Hoy::::::::: #{today}"
      # puts "Diferencia:: #{days} día(s)"
      # days > 0, el siguiente repaso se encuentra ubicado en un día en el futuro
      # days ≈ 0, el siguiente repaso es hoy
      # days < 0, el siguiente repaso se encuentra ubicado en un día en el pasado (ya pasó el día)
      return days
   end
   
   # Obtener status de la InteractedWord (Flashcard)
   def status()
      status = ''
      days = days_until_next_review()
      
      # Const::DAYS_RANGE = 1.5
      
      if (days > Const::DAYS_RANGE)
         status = Const::STANDBY_STATUS # En espera
      elsif (days <= Const::DAYS_RANGE && days >= 0)
         status = Const::NEXT_STATUS # Próxima
      elsif (days < 0)
         status = Const::EXPIRED_STATUS # Caducada
      else
         puts "Opción no válida (interacted_word.rb < status )"
      end
      
      return status
   end
   
   # Obtener dificultad de la InteractedWord (Flashcard)
   def difficulty()
      difficulty = ''
      score = self.record_repetition.to_f / self.encounters.to_f
      score += 0.10 if (self.repetitions >= 3)
      
      if (score >= 0.75)
         difficulty = Const::LOW_DIFFICULTY # Fácil
      elsif (score < 0.75 && score > 0.40)
         difficulty = Const::MEDIUM_DIFFICULTY # Intermedio
      elsif (score <= 0.40)
         difficulty = Const::HIGH_DIFFICULTY # Difícil
      else
         puts "Opción no válida (interacted_word.rb < difficulty )"
      end
      
      return difficulty
   end
   
   # Obtener una oración sin una palabra ()
   def sentence_without_word(word)
      word_mask = "_" * word.size()
      sentence = self.sentence.gsub(word, word_mask)
      
      return sentence
   end
   
end
