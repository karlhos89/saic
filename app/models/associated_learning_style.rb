class AssociatedLearningStyle < ApplicationRecord
   validates :user_id, uniqueness:true
   
   # AssociatedLearningStyle - User
   belongs_to :user
   # AssociatedLearningStyle - LearningStyle
   belongs_to :learning_style
end
