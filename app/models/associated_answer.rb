class AssociatedAnswer < ApplicationRecord
   validates :answer_id, uniqueness:true
   
   # AssociatedAnswer - Question
   belongs_to :question
   # AssociatedAnswer - Answer
   belongs_to :answer
end
