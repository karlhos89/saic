class AnsweredQuestion < ApplicationRecord
   # validates :user_id, uniqueness: {scope: :question_id message: "Sólo se puede relacionar un user/question"}
   
   # AnsweredQuestion - User
   belongs_to :user
   # AnsweredQuestion - Question
   belongs_to :question
   # AnsweredQuestion - Answer
   belongs_to :answer
end
