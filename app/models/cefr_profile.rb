class CefrProfile < ApplicationRecord
   validates :level, uniqueness:true
   
   # CEFRProfile - ProfiledUsers - Users
   has_many :profiled_users
   has_many :users, through: :profiled_users
   # CEFRProfile - ProfiledTexts - Texts
   has_many :profiled_texts
   has_many :texts, through: :profiled_texts
   
   # Nombre completo del nivel
   def level_name
    "#{level} (#{name})"
  end
end
