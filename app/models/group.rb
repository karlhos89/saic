class Group < ApplicationRecord
   validates :name, uniqueness:true
   
   # Group - GroupedUsers - Users
   has_many :grouped_users
   has_many :users, through: :grouped_users
end
