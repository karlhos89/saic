class GroupedUser < ApplicationRecord
   # GroupedUser - User
   belongs_to :user
   # GroupedUser - Group
   belongs_to :group
end
