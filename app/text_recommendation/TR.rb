#encoding: UTF-8

module TR
   # Parámetros de conexión (reemplazar con los datos del proyecto u obtenidos desde Rails)
   # HOST         = '127.0.0.1'
   # PORT         = '5432'
   # DATABASE     = 'saic_database_dev'
   # USER         = 'saic_user'
   # PASSWORD     = 'saic_user'
   # TIME_REQUEST = 10
   VERBOSE      = false # Indica si la ejecución generará log o no

   # Parámetros de formato
   UNREAD_STATUS   = 'unread' unless (const_defined?(:UNREAD_STATUS))
   READ_STATUS     = 'read' unless (const_defined?(:READ_STATUS))
   DEFAULT_QUALITY = 0.0 unless (const_defined?(:DEFAULT_QUALITY)) # Recommendation quality
   
   # Validación para el algoritmo TR: cantidad de textos recomendados y no leídos
   TEXTS_QUANTITY_VALIDATION    = 1 unless (const_defined?(:TEXTS_QUANTITY_VALIDATION))
   # Validación para el algoritmo TR: días hasta el próximo repaso
   DAYS_UNTIL_REVIEW_VALIDATION = 7 unless (const_defined?(:DAYS_UNTIL_REVIEW_VALIDATION))
   # Validación para el algoritmo TR: sólo una vez por sesión, a menos q se lean todos los textos
   ALREADY_RECOMMENDED_VALIDATION = true unless (const_defined?(:ALREADY_RECOMMENDED_VALIDATION))
   
   # Cantidad mínima de palabras con las que trabaja el algoritmoTR
   VOCABULARY_MIN_SIZE    = 5 unless (const_defined?(:VOCABULARY_MIN_SIZE))
   # Cantidad máxima de palabras con las que trabaja el algoritmoTR
   VOCABULARY_MAX_SIZE    = 20 unless (const_defined?(:VOCABULARY_MAX_SIZE))
   # TRUE: remover de la lista de stopwords aquellas palabras en el vocabulario del usuario
   REMOVE_STOPWORDS       = true unless (const_defined?(:REMOVE_STOPWORDS))
   # Indica si la recomendación de textos se hace mediante lemas o palabras
   LEMMA_ANALYSIS         = true unless (const_defined?(:LEMMA_ANALYSIS))
   # Cantidad de decimales con los que trabajará el algorimoTR
   DECIMALS               = 15 unless (const_defined?(:DECIMALS))
   # Límite TF-IDF para obtener una lista de stopwords de los textos
   STOPWORDS_SCORE_LIMIT  = 0.01 unless (const_defined?(:STOPWORDS_SCORE_LIMIT))
   # Cantidad máxima de textos recomendados por el algoritmoTR
   RECOMMENDED_TEXT_LIMIT = 10 unless (const_defined?(:RECOMMENDED_TEXT_LIMIT))
   # Lista de stopwords: Python NLTK + stopwords del MCTest-corpus
   STOPWORDS_LIST = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', 'should', 'now', 'never', 'without', 'would', 'next', 'even', 'best', 'top', 'bottom', 'go', 'goes', 'going', 'done', 'went', 'someone', 'somebody', 'somewhere', 'something', 'sometimes', 'always', 'back', 'could', 'around', 'really', 'make', 'makes', 'get', 'gets', 'got', 'every', 'everyone', 'everybody', 'everywhere', 'everything', 'like', 'liked', 'mom', 'mommy', 'also', 'ouch', 'one', 'first', 'long', 'longer', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'january', 'february', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december', 'new', 'day', 'days', 'put', 'puts', 'real', 'thought', 'good', 'today', 'home', 'said', 'asked', 'time', 'saw', 'gave', 'looked', 'house', 'big', 'see', 'take', 'come', 'came', 'play', 'made', 'friends', 'wanted', 'happy', 'took'] unless (const_defined?(:STOPWORDS_LIST))
end