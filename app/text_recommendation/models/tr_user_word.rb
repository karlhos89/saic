#encoding: UTF-8
require_relative "../TR"

class TRUserWord
	# UserWord
	attr_accessor :id
   attr_accessor :word
   attr_accessor :lemma
   # InteractedWord / User
	attr_accessor :word_es
	attr_accessor :sentence
	attr_accessor :word_image
	attr_accessor :repetitions
	attr_accessor :encounters
	attr_accessor :record_repetition
	attr_accessor :user_first_review
	attr_accessor :user_last_review
	attr_accessor :user_next_review
	attr_accessor :probability
   attr_accessor :text_id
   # Extras
	attr_accessor :scores_array # puntajes TD*IDF
	
	
	## Public: inicializa el modelo de datos
	# 
	# id                :: Identificador (integer)
	# word              :: Palabra en inglés (string)
	# word_es           :: Palabra en español (string)
	# sentence          :: Oración en la cual se encontró la palabra en inglés (string)
	# image             :: Imagen representativa de la palabra (string url)
	# repetitions       :: Número de repeticiones correctas y continuas (integer)
	# encounters        :: Número de encuentros con la palabra (integer)
	# record_repetition :: Número de repasos correctos, no consecutivos (integer)
	# user_first_review :: Fecha y hora de la 1a revisión de la palabra (DateTime) (AAAA-MM-DD ZZ)
	# user_last_review  :: Fecha y hora de la última revisión de la palabra (Date)
   # user_next_review  :: Fecha y hora de la siguiente revisión de la palabra (Date)
   # probability       :: Probabilidad con la que se calcula user_next_review (default: 0.5)
	# text_id           :: Identificador donde se encontró la palabra (integer)
	# score_array       :: Puntajes TF-IDF de los textos donde se encuentra la palabra (array)
	def initialize(id, word, lemma, word_es, sentence, image, repetitions, encounters, record_repetition, user_first_review, user_last_review, user_next_review, probability, text_id, scores_array = Array.new())
		@id                = id
      @word              = word
      @lemma             = lemma
      
		@word_es           = word_es
		@sentence          = sentence
		@word_image        = image
		@repetitions       = repetitions
		@encounters        = encounters
		@record_repetition = record_repetition
		@user_first_review = user_first_review
		@user_last_review  = user_last_review
		@user_next_review  = user_next_review
		@probability       = probability
      @text_id           = text_id
      
		# Arreglo
		@scores_array      = scores_array
	end
	
	## Public: calcular el promedio TF*IDF para los puntajes obtenidos en @scores_array
	#
	def tf_idf()
		summation = 0.0
		
		scores_array.each do |tf_idf|
			summation = summation + tf_idf
		end
		
		# Obtener la media aritmética
		return (summation.to_f / scores_array.size.to_f).round(TR::DECIMALS)
	end
	
end