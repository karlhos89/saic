#encoding: UTF-8

class TRText
	# Text
	attr_accessor :id
	attr_accessor :title
	attr_accessor :body
	attr_accessor :tokens
   attr_accessor :types
   attr_accessor :lemmas
	# ProfiledText / CefrProfile
	attr_accessor :cefr_id
	attr_accessor :cefr_level
	attr_accessor :cefr_name
	attr_accessor :cefr_description
	# AssociatedText / User
	attr_accessor :status
	attr_accessor :recommendation
   attr_accessor :recommendation_quality
   attr_accessor :similarity # recommendation_quality para insert y delete
   # AssociatedWord / TextWord
	attr_accessor :vocabulary # Lista <TextWord>
	attr_accessor :top_words # Lista <TextWord>
	
	
	## Public: inicializa el modelo de datos
	# 
	# id                     :: Identificador (integer)
	# title                  :: Título del texto (string)
	# body                   :: Texto (string)
	# tokens                 :: Cantidad de palabras en el texto (int)
	# types                  :: Cantidad de palabras diferentes en el texto (int)
	# lemmas                 :: Cantidad de palabras no flexivas y diferentes en el texto (int)
	# cefr_id                :: Identificador del nivel MCER (integer)
	# cefr_level             :: Nivel MCER (string)
	# cefr_name              :: Nombre del nivel MCER (string)
	# cefr_description       :: Descripción del nivel MCER (string)
	# status                 :: Estado del texto: unread / read (string)
	# recommendation         :: Si el texto es recomendado, true. De lo contrario, false (bool)
	# recommendation_quality :: Puntaje de calidad de la recomendación (similarity) (float)
	# similarity             :: Similitud semántica voc. texto vs voc. usuario (float)
	# vocabulary             :: Vocabulario del texto (array<TextWord>)
	# top_words              :: Lista de palabras que son 'creativity words' (array<TextWord>)
	def initialize(id, title, text, tokens, types, lemmas, status, recommendation, recommendation_quality, cefr_id, cefr_level, cefr_name, cefr_description, similarity = 0.0, vocabulary = Array.new(), top_words = Array.new())
		@id                     = id
		@title                  = title
		@text                   = text
		@tokens                 = tokens
      @types                  = types
      @lemmas                 = lemmas
      
		@status                 = status
		@recommendation         = recommendation
      @recommendation_quality = recommendation_quality
      
		@cefr_id                = cefr_id
		@cefr_level             = cefr_level
		@cefr_name              = cefr_name
      @cefr_description       = cefr_description
      
      @simularity             = similarity
      
		# Arreglos
		@vocabulary             = vocabulary
		@top_words              = top_words
	end
	
end