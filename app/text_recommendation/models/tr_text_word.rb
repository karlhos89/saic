#encoding: UTF-8

class TRTextWord
	# TextWord
	attr_accessor :id
   attr_accessor :word
   attr_accessor :lemma
   # AssociatedWord / Text
   attr_accessor :word_frequency
   attr_accessor :lemma_frequency
   attr_accessor :top_word # true/false
   # Extras
	attr_accessor :tf
	attr_accessor :idf
	attr_accessor :tf_idf
	
	
	## Public: inicializa el modelo de datos
	# 
	# id              :: Identificador (int)
   # word            :: Palabra en inglés (string)
   # lemma           :: Palabra no flexiva (string)
   # word_frequency  :: Número de veces que aparece la palabra en el texto (int)
   # lemma_frequency :: Número de veces que aparece el lema en el texto (int)
	# top_word        :: Indica si la palabra es una creativity word (bool)
	# tf              :: Puntaje TF (float)
	# idf             :: Puntaje IDF (float)
	# tf_idf          :: Puntaje TF-IDF (float)
	def initialize(id, word, lemma, word_frequency, lemma_frequency, top_word, tf = 0.0, idf = 0.0, tf_idf = 0.0)
		@id              = id
      @word            = word
      @lemma           = lemma
      
      @word_frequency  = word_frequency
      @lemma_frequency = lemma_frequency
      @top_word        = top_word
      
		@tf              = tf
		@idf             = idf
		@tf_idf          = tf_idf
	end
	
end