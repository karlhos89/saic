#encoding: UTF-8

class TRUser
	# User
   attr_accessor :id
   attr_accessor :email
	attr_accessor :first_name
	attr_accessor :last_name
	attr_accessor :birthdate
   # ProfiledUser / CefrProfile
	attr_accessor :cefr_id
	attr_accessor :cefr_level
	attr_accessor :cefr_name
   attr_accessor :cefr_description
   # AssociatedGender / Gender
	attr_accessor :gender_id
	attr_accessor :gender_name
   attr_accessor :gender_description
   # AssociatedLearningStyle / LearningStyle
	attr_accessor :style_id
	attr_accessor :style_name
   attr_accessor :style_description
   # InteractedWord / UserWord
	attr_accessor :vocabulary # Lista <UserWord>
	attr_accessor :texts # Lista <Text>
	
	
	## Public: inicializa el modelo de datos
	# 
	# id                 :: Identificador (int)
	# email              :: Correo electrónico (string)
	# first_name         :: Nombre (string)
	# last_name          :: Apellido (string)
	# birthday           :: Fecha de nacimiento (Date) (Format: AAAA-MM-DD)
	# password           :: Contraseña (string sin encriptar)
	# help               :: Bandera de ayuda (integer)
	# cerf_id            :: Identificador del nivel MCER (integer)
	# cefr_level         :: Nivel MCER (string)
	# cefr_name          :: Nombre del nivel MCER (string)
	# cefr_description   :: Descripción del nivel MCER (string)
	# gender_id          :: Identificador del género (integer)
	# gender_name        :: Nombre del género (string)
	# gender_description :: Descripción del género (string)
	# style_id           :: Identificador del estilo de aprendizaje (integer)
	# style_name         :: Nombre del estilo de aprendizaje (string)
	# style_description  :: Descripción del estilo de aprendizaje (string)
	def initialize(id, email, first_name, last_name, birthdate, cefr_id, cefr_level, cefr_name, cefr_description, gender_id, gender_name, gender_description, style_id, style_name, style_description, vocabulary = Array.new(), texts = Array.new())
      @id                 = id
      @email              = email
		@first_name         = first_name
		@last_name          = last_name
      @birthdate          = birthdate
      
		@cefr_id            = cefr_id
		@cefr_level         = cefr_level
		@cefr_name          = cefr_name
      @cefr_description   = cefr_description
      
		@gender_id          = gender_id
		@gender_name        = gender_name
      @gender_description = gender_description
      
		@style_id           = style_id
		@style_name         = style_name
		@style_description  = style_description
		# Arreglos
		@vocabulary         = vocabulary
		@texts              = texts
	end
	
end