#encoding: UTF-8
require_relative "models/tr_text"
require_relative "models/tr_text_word"
require_relative "models/tr_user"
require_relative "models/tr_user_word"
require_relative "TR"

class TRRequestAdapter

	## Public: Constructor
	def initialize()
	end
	
	## SELECT
	
	## Public: consulta para obtener el vocabulario del usuario
	# 20 palabras con el 'próximo repaso' más antiguo
	#
	# user_id :: Identificador del usuario (int)
	def obtain_user_vocabulary(user_id)
		puts "\n > obtener TRUserWords del usuario (#{TR::VOCABULARY_MAX_SIZE} palabras más antiguas)"  if (TR::VERBOSE)
		user_vocabulary = Array.new()
		user_word = nil
		
		query = "SELECT \"user_id\", \"user_word_id\", \"text_id\", \"word\", \"lemma\", \"word_es\", \"sentence\", \"word_image\", \"repetitions\", \"encounters\", \"record_repetition\", \"user_first_review\", \"user_last_review\", \"user_next_review\", \"probability\" " +

		"FROM \"interacted_words\" " +

		"JOIN \"user_words\" ON \"user_words\".\"id\" = \"interacted_words\".\"user_word_id\" " +
		"JOIN \"users\" ON \"users\".\"id\" = \"interacted_words\".\"user_id\" " +

      "WHERE \"users\".\"id\" = \'#{user_id}\' " +

      "ORDER BY \"user_next_review\" ASC " +
      
      "LIMIT #{TR::VOCABULARY_MAX_SIZE};"
      
		data = request_query(query)
      
		data.each do |row|
			id = row["user_word_id"].to_s
         word = row["word"].to_s
         lemma = row["lemma"].to_s
			word_es = row["word_es"].to_s
			sentence = row["sentence"].to_s
			word_image = row["word_image"]
			repetitions = row["repetitions"].to_i
			encounters = row["encounters"].to_i
			record_repetition = row["record_repetition"].to_i
			user_first_review = row["user_first_review"].to_s
			user_last_review = row["user_last_review"].to_s
			user_next_review = row["user_next_review"].to_s
			probability = row["probability"].to_s
			text_id = row["text_id"].to_s

			user_word = TRUserWord.new(id, word, lemma, word_es, sentence, word_image, repetitions, encounters, record_repetition, user_first_review, user_last_review, user_next_review, probability, text_id, Array.new())

			user_vocabulary.insert(-1, user_word)
		end

		puts " TRUserWord obtenidas: #{user_vocabulary.size()}" if (TR::VERBOSE)

		return user_vocabulary
	end
   
	## Public: consulta para obtener los textos del usuario
	# Textos previamente asociados y no léidos
	#
	# user_id :: Identificador del usuario (int)
	def obtain_user_texts(user_id)
		puts "\n > obtener TRTexts associados al usuario y no leídos"  if (TR::VERBOSE)
		texts_list = Array.new()
		text = nil

		query = "SELECT  \"texts\".\"id\", \"title\", \"body\", \"tokens\", \"types\", \"lemmas\", \"status\", \"recommendation\", \"recommendation_quality\", \"cefr_profiles\".\"id\", \"level\", \"name\", \"description\" " +
		
		"FROM \"texts\" " +

		"FULL JOIN \"associated_texts\" ON \"associated_texts\".\"text_id\" = \"texts\".\"id\" " +
		"FULL JOIN \"users\" ON \"users\".\"id\" = \"associated_texts\".\"user_id\" " +

		"FULL JOIN \"profiled_texts\" ON \"profiled_texts\".\"text_id\" = \"texts\".\"id\" " +
		"FULL JOIN \"cefr_profiles\" ON \"cefr_profiles\".\"id\" = \"profiled_texts\".\"cefr_profile_id\" " +

		"WHERE (\"users\".\"id\" = \'#{user_id}\') " +
		"AND (\"associated_texts\".\"status\" = \'unread\') " +
			
		"ORDER BY \"texts\".\"id\" ASC;"

		data = request_query(query)
      
		data.each_row do |row|
			# puts row
			id = row[0].to_s
			title = row[1].to_s
			text = row[2].to_s
			tokens = row[3].to_i
         types = row[4].to_i
         lemmas = row[5].to_i
			status = (row[6] == nil ? 'unread' : row[6].to_s)
			recommendation = (row[7].to_s == 't' ? true : false)
			recommendation_quality = (row[8] == nil ? 0.0 : row[8].to_f)
			cefr_id = row[9].to_s
			cefr_level = row[10].to_s
			cefr_name = row[11].to_s
			cefr_description = row[12].to_s

			text = TRText.new(id, title, text, tokens, types, lemmas, status, recommendation, recommendation_quality, cefr_id, cefr_level, cefr_name, cefr_description)

			texts_list.insert(-1, text)
		end

		puts " TRText obtenidos: #{texts_list.size()}" if (TR::VERBOSE)

		return texts_list
	end
	
	## Public: consulta para obtener el vocabulario de un texto
	#
	# text_id :: Identificador del texto (int)
	def obtain_text_vocabulary(text_id)
		puts "\n > obtener TRTextWord de un TRText"  if (TR::VERBOSE)
		text_vocabulary = Array.new()
		system_word = nil

		query = "SELECT \"text_words\".\"id\", \"word\", \"lemma\", \"word_frequency\", \"lemma_frequency\", \"top_word\" " +
		
		"FROM \"text_words\" " +

		"JOIN \"associated_words\" ON \"associated_words\".\"text_word_id\" = \"text_words\".\"id\" " +
		"JOIN \"texts\" ON \"texts\".\"id\" = \"associated_words\".\"text_id\" " +

		"WHERE \"texts\".\"id\" = #{text_id} " +

		"ORDER BY \"top_word\" DESC;"

		data = request_query(query)
      
		data.each do |row|
         id = row["id"].to_s
         word = row["word"].to_s
         lemma = row["lemma"].to_s
         word_frequency = row["word_frequency"].to_i
         lemma_frequency = row["lemma_frequency"].to_i
			top_word = (row["top_word"].to_s == 't' ? true : false)

			system_word = TRTextWord.new(id, word, lemma, word_frequency, lemma_frequency, top_word)

			text_vocabulary.insert(-1, system_word)
		end

		puts " TRTextWord obtenidas: #{text_vocabulary.size()}" if (TR::VERBOSE)

		return text_vocabulary
	end
	
	## UPDATE
	
	## Public: limpiar las recomendaciones (en AssociatedText) de un usuario
	# 
	# user_id :: Identificador del usuario (int)
	def clean_recommendations(user_id)
		puts "\n > Limpiar recomendaciones de texto" if (TR::VERBOSE)
		
		query = "UPDATE \"associated_texts\" "+
	
		"SET \"recommendation\" = false, \"recommendation_quality\" = #{TR::DEFAULT_QUALITY} "+
	
		"WHERE \"user_id\" = #{user_id};"
		
		pg_result = request_query(query)
		return false if (pg_result.nil?) # Prevenir excepción si el resultado es nulo
	
		return pg_result.cmd_tuples() == 0 ? false : true # Comprobar q se alteró mínimo 1 registro
	end
	
	## Public: actualizar un AssociatedText de un usuario
	#
	# text    :: Objeto del tipo TRText (TRText)
	# user_id :: Identificador del usuario (int)
	def update_associated_text(text, user_id)
		puts "\n > actualizar AssociatedText con datos de TRText" if (TR::VERBOSE)
		# status = text.status.size > 0 ? text.status : TR::UNREAD_STATUS
		
		query = "UPDATE \"associated_texts\" "+
		
		"SET \"recommendation\" = #{text.recommendation}, \"recommendation_quality\" = #{text.recommendation_quality} "+
		
		"WHERE (\"user_id\" = #{user_id} AND \"text_id\" = #{text.id});"
		
		pg_result = request_query(query)
		return false if (pg_result.nil?) # Prevenir excepción si el resultado es nulo
		
		return pg_result.cmd_tuples() == 0 ? false : true # Comprobar que se alteró mínimo 1 registro
		# puts pg_result.error_message() # Mensaje si hubo algún error en la instrucción
		# puts pg_result.cmd_tuples() # Número de registros afectados por la instrucción
	end
	
	# REQUEST
	
	### Public: realiza una conexión/petición a base de datos PostgreSQL
	#
	# query :: Una instrucción SQL válida (string)
	def request_query(query)
		# Versión de Gem PG para una conexión Ruby/PostgreSQL
		version = PG.library_version.to_s
		version.gsub!("0", ".")
		puts "\nVersión de libpg (GEM PostgreSQL): #{version}" if (TR::VERBOSE)

		begin
			# Datos obtenidos desde la configuración de Rails
			config   = Rails.configuration.database_configuration
			host     = config[Rails.env]["host"]
			port     = config[Rails.env]["port"]
			timeout  = config[Rails.env]["timeout"]
			database = config[Rails.env]["database"]
			username = config[Rails.env]["username"]
			password = config[Rails.env]["password"]
			
			# Objeto de conexión, el objeto es un tipo HASH
			puts "DBMS PostgreSQL: Conectando..." if (TR::VERBOSE)
			connection = PG::Connection.new(:hostaddr => host, :port => port, :dbname => database, :user => username, :password => password, :connect_timeout => timeout)
			puts "DBMS PostgreSQL: Conexión lista.\n" if (TR::VERBOSE)

			puts query if (TR::VERBOSE)
			data = connection.exec(query)

			return data
		rescue PG::Error => error
			puts "\nDBMS PostgreSQL: Error (#{error.message})"
			# retry # This will move control to the beginning of begin
			return nil
		else
			puts "\nDBMS PostgreSQL: OK\n" if (TR::VERBOSE)
		ensure
			if (connection)
				connection.close()
				puts "DBMS PostgreSQL: Conexión terminada.\n" if (TR::VERBOSE)
			end
		end
	end
	
end