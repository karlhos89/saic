#encoding: UTF-8
require_relative "models/tr_text"
require_relative "models/tr_text_word"
require_relative "models/tr_user"
require_relative "models/tr_user_word"
require_relative "tr_request_adapter"
require_relative "TR"

class TextRecommendation

	## Public: constructor
	def initialize()
		@stopwords = Array.new(TR::STOPWORDS_LIST)
		@request_adapter = TRRequestAdapter.new()
	end
	
	
	## Public: generar recomendaciones de texto para el usuario proporcionado
	#
	# user :: Datos del usuario (TRUser.rb)
	# @return Devuelve un Array<TRText>. En caso de error, devuelve nil.
	def generate_text_recommendations(user)
		puts '\nGenerar Recomendaciones de Texto:'
		user_word = nil
		text_word = nil
		
		# (1) Obtener VOCABULARIO DEL USUARIO (20 palabras con user_next_review más antiguo)
		puts '(1) Obtener VOCABULARIO DEL USUARIO desde base de datos'
		user.vocabulary = @request_adapter.obtain_user_vocabulary(user.id)
		
		if (user.vocabulary.size >= TR::VOCABULARY_MIN_SIZE)
			
			# (2) Obtener TEXTOS DEL USUARIO (Textos previamente asociados y no leídos)
			puts '(2) Obtener TEXTOS DEL USUARIO desde base de datos'
			user.texts = @request_adapter.obtain_user_texts(user.id)
			
			# Cantidad de textos recuperados
			texts_quantity = 0
			if (user.texts.size() > 0)
				texts_quantity = user.texts.size()
			else
				puts " Textos insuficientes (#{user.texts.size()} textos)"
				return nil
			end
			
			# (3) Obtener VOCABULARIO de cada texto obtenido
			puts '(3) Obtener VOCABULARIO de cada texto obtenido'
			user.texts.each do |text|				
				# Obtener vocabulario de cada texto
				text.vocabulary = @request_adapter.obtain_text_vocabulary(text.id)
				
				# Obtener top_words de cada texto
				i = 0
				while (i < text.vocabulary.size())
					text_word = text.vocabulary[i]
					text.top_words.insert(-1, text_word) if (text_word.top_word)
					i += 1
				end
				# puts "TopWords: #{text.top_words.size}"
			end
			
			# (4) Remover de la LISTA DE STOPWORDS aquellas palabras en el vocabulario del usuario
			puts '(4) Remover de la LISTA DE STOPWORDS aquellas palabras en el vocabulario del usuario'
			i = 0
			while ((i < user.vocabulary.size()) && TR::REMOVE_STOPWORDS)
				user_word = user.vocabulary[i]
				@stopwords.delete(user_word.word)
				i += 1
			end
			
			# (5) Generar ESPACIO VECTORIAL del vocabulario de cada texto
			puts '(5) Generar ESPACIO VECTORIAL del vocabulario de cada texto'
			user.texts.each do |text|
				i = 0
				
				while (i < text.vocabulary.size())
					text_word = text.vocabulary[i]
					
					unless (@stopwords.member?(text_word.word))
						
						if (TR::LEMMA_ANALYSIS)
							# TF = frecuencia del lema en el texto / total de lemas en el texto
							text_word.tf = (text_word.lemma_frequency.to_f / text.lemmas.to_f).round(TR::DECIMALS)
							# IDF = cantidad de textos / cantidad de textos donde aparece el lema
							word_encounters = count_texts_with_lemma(text_word.lemma, user.texts)
							text_word.idf = (texts_quantity.to_f / word_encounters.to_f).round(TR::DECIMALS)
						else
							# TF = frecuencia de la palabra en el texto / total de palabras en el texto
							text_word.tf = (text_word.word_frequency.to_f / text.tokens.to_f).round(TR::DECIMALS)
							# IDF = cantidad de textos / cantidad de textos donde aparece la palabra
							word_encounters = count_texts_with_word(text_word.word, user.texts)
							text_word.idf = (texts_quantity.to_f / word_encounters.to_f).round(TR::DECIMALS)
						end
						
						# TF*IDF
						text_word.tf_idf = (text_word.tf.to_f * text_word.idf.to_f).round(TR::DECIMALS)
						
						# Guardar datos de vectorización TF*IDF en TRText.vocabulary
						text.vocabulary[i] = text_word
						
						# Obtener STOPWORDs del corpus
						# stopword = text_word.tf_idf < STOPWORDS_LIMIT_SCORE ? "STOPWORD: #{text_word.word}" : nil
						# unless (stopword == nil)
						# 	puts stopword
						# end
						
						# puts "#{text_word.word} #{text_word.tf_idf()}"
						# puts "#{text_word.lemma} #{text_word.tf_idf()}"
					end
					
					i += 1
				end
			end
			
			# (6) Generar ESPACIO VECTORIAL del vocabulario del usuario
			puts '(6) Generar ESPACIO VECTORIAL del vocabulario del usuario'
			i = 0
			while (i < user.vocabulary.size)
				user_word = user.vocabulary[i]
					
				user.texts.each do |text|
					text_word = text.vocabulary.find { |tw| tw.word == user_word.word}
					
					if (text_word)
						user_word.scores_array.insert(-1, text_word.tf_idf)
					end
				end
				
				i += 1 # Incrementa el contador
			end
			
			# (7) Calcular SIMILITUD SEMÁNTICA de los vocabularios: usuario vs textos
			puts '(7) Calcular SIMILITUD SEMÁNTICA de los vocabularios: usuario vs textos'
			
			# Obtener arreglo del espacio vectorial del vocabulario del usuario
			user_vector_space = Array.new()
			
			user.vocabulary.each do |user_word|
				if (user_word.tf_idf() > 0.0)
					# puts "Palabra agregada: #{user_word.word} score: #{user_word.tf_idf()}"
					user_vector_space.insert(-1, user_word.tf_idf())
				end
			end
			
			# Obtener arreglo del espacio vectorial del vocabulario de cada texto
			user.texts.each do |text|
				text_vector_space = Array.new()
				
				text.vocabulary.each do |text_word|
					if (text_word.tf_idf > 0.0)
						# puts "Palabra agregada: #{text_word.word} score: #{text_word.tf_idf}"
						text_vector_space.insert(-1, text_word.tf_idf)
					end
				end
				
				# Obtener similitud semántica entre los vectores: usuario vs texto
				text.similarity = semantic_similarity(user_vector_space, text_vector_space)
			end
			
			# (8) Obtener TEXTOS SUGERIDOS con la similitud semántica más alta
			puts '(8) Obtener TEXTOS SUGERIDOS con la similitud semántica más alta'
			recommended_texts = obtain_texts_most_similarity(user.texts)
			
			recommended_texts.each do |text|
				puts "TextID: #{text.id} Score: #{text.similarity}"
				text.similarity = 0 if (text.similarity.nan?) # NaN es un valor no válido resultado de una operación como 0/0
				puts "Corrección > TextID: #{text.id} Score: #{text.similarity}"
			end
			
			return recommended_texts
		else
			puts " Vocabulario: insuficiente (#{user.vocabulary.size()} palabras)"
			return nil
		end
		
		return nil
	end
	
	## Public: Actualiza/Inserta las recomendaciones de texto generadas para el usuario
	#
	# texts :: Array de textos a los que el usuario tiene acceso (Array<TRText>)
	# user  :: Identificador del usuario (string)
	def update_text_recommendations(texts, user)
		modified_rows = 0
		
		# El método devuelve false si no hay textos recomendados disponibles
		if (texts == nil || texts.size() == 0)
			puts 'No hay textos recomendados para actualizar'
			return false
		end
		
		# Limpiar las recomendaciones antiguas
		@request_adapter.clean_recommendations(user.id)		
		
		texts.each do |text|
			# Actualizar registro de AssociatedText con datos de text para user
			is_update = @request_adapter.update_associated_text(text, user.id)
			# No se actualiza el registro si no existe, en ese caso se inserta un registro nuevo
			# unless (is_update)
			# 	is_insert = @request_adapter.insertAssociatedText(text, user.id)	
			# end
			# modified_rows = (is_update || is_insert) ? modified_rows += 1 : modified_rows
			modified_rows = is_update ? modified_rows += 1 : modified_rows
		end
		
		puts "Recomendaciones actualizadas: #{modified_rows} textos"
		return modified_rows == 0 ? false : true
	end
	
	
	private
	
	
	## Private: contar el número de textos de una colección en los que aparece un lema
	#
	# lemma :: Lema que se va a buscar en la colección (string)
	# texts :: Arreglo|Colección de textos (Array<TRText>)
	def count_texts_with_lemma(lemma, texts)
		text_word = nil
		count = 0
		
		texts.each do |text|
			text_word = text.vocabulary.find { |tw| tw.lemma == lemma }
			
			if (text_word)
				count += 1
			end
		end
		
		# puts "Lema encontrado: #{lemma} (#{count})"
		return count
	end
	
	## Private: contar el número de textos de una colección en los que aparece una palabra
	#
	# word  :: Palabra que se va a buscar en la colección (string)
	# texts :: Arreglo de textos (Array<TRText>)
	def count_texts_with_word(word, texts)
		text_word = nil
		count = 0
		
		texts.each do |text|
			text_word = text.vocabulary.find { |tw| tw.word == word } # Primera coincidencia
			# text_word = text.vocabulary.select { |sw| sw.word == word } # Devuelve array de las coincidencias, también utiliza el alias .find_all
			if (text_word)
				count += 1
			end
		end
		
		# puts "Palabra encontrada: #{word} (#{count})"
		return count
	end
	
	## Private: calcular similitud semántica de los vocabularios: usuario vs texto
	#
	# user_vector_space :: Arreglo de puntajes TF*IDF del vocabulario del usuario (Array<float>)
	# text_vector_space :: Arreglo de puntajes TF*IDF del vocabulario de un texto (Array<float>)
	def semantic_similarity(user_vector_space, text_vector_space)
		# Crear una copia de los array originales
		user_vspace = Array.new(user_vector_space)
		text_vspace = Array.new(text_vector_space)
		
		# Homologar arreglos para realizar las sumatorias en un ciclo
		if (user_vspace.size() < text_vspace.size())
			difference = text_vspace.size() - user_vspace.size()
			user_vspace.concat(Array.new(difference, 0.0))
			# puts "user_vspace: #{user_vspace.size} AND text_vspace: #{text_vspace.size}"
		elsif (user_vspace.size() > text_vspace.size())
			difference = user_vspace.size - text_vspace.size
			text_vspace.concat(Array.new(difference, 0.0))
			# puts "text_vspace: #{user_vspace.size} AND user_vspace: #{text_vspace.size}"
		end
		
		# Ecuación de similitud coseno para determinar la similitud semántica de dos vectores
		i = 0
		sum_numerator = 0.0 # Sumatoria del númerador
		user_sum_denominator = 0.0 # Sumatoria del denominador (user vector)
		text_sum_denominator = 0.0 # Sumatoria del denominador (text vector)
		
		while (i < user_vspace.size)
			sum_numerator = sum_numerator.to_f + (user_vspace[i].to_f * text_vspace[i].to_f).round(TR::DECIMALS)
			
			user_sum_denominator = user_sum_denominator.to_f + (user_vspace[i].to_f * user_vspace[i].to_f).round(TR::DECIMALS)
			text_sum_denominator = text_sum_denominator.to_f + (text_vspace[i].to_f * text_vspace[i].to_f).round(TR::DECIMALS)
			
			i += 1
		end
		
		user_sum_denominator = Math.sqrt(user_sum_denominator)
		text_sum_denominator = Math.sqrt(text_sum_denominator)
		# puts "Numerador::::: #{sum_numerator}"
		# puts "Denominador u: #{user_sum_denominator}"
		# puts "Denominador t: #{text_sum_denominator}"
		similarity = (sum_numerator.to_f / (user_sum_denominator.to_f * text_sum_denominator.to_f)).round(TR::DECIMALS)
		# puts "Similitud semántica: #{similarity}"
		
		return similarity
	end
	
	## Private: obtener los textos con la mejor similitud semántica posible
	#
	# texts :: Arreglo de textos con puntaje de similitud semántica (Array<Text>)
	def obtain_texts_most_similarity(texts)		
		summation = 0.0
		# most_similarity_text = nil
		
		texts.each do |text|
			# Obtener sumatoria de media aritmética
			summation = (summation.to_f + text.similarity.to_f).round(TR::DECIMALS)
			
			# Asignar parametros de recomendación
			text.recommendation = true
			text.recommendation_quality = text.similarity.to_f
			
			# most_similarity_text = (most_similarity_text == nil) ? text : most_similarity_text
			# most_similarity_text = most_similarity_text.similarity < text.similarity ? text : most_similarity_text
		end
		
		# Media aritmética de todas las medidas de similitud (TF*IDF)
		arithmetic_mean = (summation.to_f / texts.size().to_f).round(TR::DECIMALS)
		
		# Ordenar textos por similitud semántica (de mayor a menor)
		sort_texts = texts.sort_by { |text| text.similarity}.reverse()
		
		# Remover textos con similitud por debajo de la media aritmética
		sort_texts.delete_if { |text| text.similarity < arithmetic_mean }
		
		# Obtener una cantidad 'limite' de textos con la mejor similitud semántica
		# u obtener todos los textos si la cantidad 'limite' es mayor al total de textos 
		sort_texts = (sort_texts.size() > TR::RECOMMENDED_TEXT_LIMIT) ? sort_texts = sort_texts[0..TR::RECOMMENDED_TEXT_LIMIT-1] : sort_texts
		
		return sort_texts
	end
	
end