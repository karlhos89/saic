// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")

// KHS: instalar bootstrap, jquery y popper.js
// yarn add bootstrap jquery popper.js
// compilar webpack
// ./bin/webpack-dev-server
// e importar como sigue:
import $ from 'jquery'
import popper from 'popper.js'
import bootstrap from 'bootstrap'
window.$ = window.jQuery = $
window.bootstrap = bootstrap
window.popper = popper
// KHS: crear carpeta y archivo (css/styless.scss)
// e importarlo como sigue:
import 'css/styles';


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)