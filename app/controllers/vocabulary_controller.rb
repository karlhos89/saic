#encoding: UTF-8
require 'base64'
require_relative '../spaced_repetition/spaced_repetition'
# KHS: Controlador para readings
class VocabularyController < ApplicationController
   before_action :authenticate_user!
   # Fix: Can't verify CSRF token authenticity.
   # protect_from_forgery :except => [:search_translate, :search_images, :add_word]
   
   # Internal Services JS
   
   def add_word_from_reading()
      # puts "#{params[:authenticity_token]} == #{session[:_csrf_token]}"
      if (params[:authenticity_token] == session[:_csrf_token])
         word          = params[:word].downcase()
         word_es       = params[:word_es].downcase()
         word_position = params[:word_position]
         word_image    = params[:word_image]
         text_id       = params[:text_id]
         
         # Obtener lemma de la palabra
         lemma = UserWord.lemmatize(Const::LEMMATIZE_WORD, word)
         # puts "LEMMA: #{lemma}"
         
         # Obtener oración con la palabra desde su texto
         text = Text.find(text_id)
         sentence = text.build_sentence(word_position.to_i)
         # puts "SENTENCE: #{sentence}"
         
         # UserWord buscar/obtener o crear
         user_word = UserWord.find_or_create_by(word:word, lemma:lemma)
         # puts "USERWORD: #{user_word.word} / #{user_word.lemma}"
         
         # InteractedWord existe o no?
         unless (InteractedWord.where(user_word_id:user_word.id, user_id:current_user.id, word_es:word_es).any?)
            # El InteractedWord consultado no existe, entonces crear uno nuevo
            
            # Crear objeto especial SRInteractedWord para el Algoritmo SR
            sr_interacted_word = SRInteractedWord.new(
               0, # id
               0, # repetitions
               0, # encounters
               0, # record_repetition
               InteractedWord.datetime_to_text(DateTime.now()), # user_first_review
               InteractedWord.datetime_to_text(DateTime.now()), # user_last_review
               0, # user_next_review
               0.0 # probability
            )
            # Instanciar la clase del Algoritmo SR
            spaced_repetition = SpacedRepetition.new()
            # Invocar el cálculo de la siguiente fecha de repaso de la palabra
            sr_interacted_word = spaced_repetition.calculate_next_review(sr_interacted_word, SR::DEFAULT_HOUR, SR::CORRECT_REVIEW)
            
            interacted_word = InteractedWord.create(
               word_es:word_es,
               sentence:sentence,
               word_image:word_image,
               repetitions:sr_interacted_word.repetitions,
               encounters:sr_interacted_word.encounters,
               record_repetition:sr_interacted_word.record_repetition,
               user_first_review:sr_interacted_word.user_first_review,
               user_last_review:sr_interacted_word.user_last_review,
               user_next_review:sr_interacted_word.user_next_review,
               probability:sr_interacted_word.probability,
               user_word_id:user_word.id,
               user_id:current_user.id,
               text_id:text_id
            )
            
            if (interacted_word.valid?)
               @message = "<b>#{word} (#{word_es})</b> se ha guardado en tu vocabulario."
            else
               @message = "Error: el servicio Add Vocabulary no está disponible, no se ha podido guardar la palabra '#{word}/#{word_es}', intenta más tarde."
               puts "Error add_word: \n#{create_alert(interacted_word)}"
            end
         else
            # El InteractedWord consultado exite, entonces mandar mensaje de que ya existe
            @message = "<b>#{word} (#{word_es})</b> ya existe en tu vocabulario."
         end
      else
         @message = "Error: tu sesión ya no es válida, no se ha podido guardar la palabra <b>#{word} (#{word_es})</b>. Cierra sesión y vuelve a entrar en la plataforma."
               puts "Error add_word: Token no válido (#{params[:authenticity_token]})"
      end
   end
   
   # Internal Services Rails
   
   def update_word_from_flashcard()
         # Obtener parametros desde petición     
         flashcards_filter  = params[:filter]
         flashcards_size    = params[:size].to_i
         flashcards_index   = params[:index].to_i
         interacted_word_id = params[:interacted_word_id].to_s
         user_response      = params[:user_response].strip().downcase()
         ranking            = params[:ranking].to_i
         exclude            = params[:exclude].to_s
         
         puts "EXCLUDE: #{exclude}"
         
         # Validación del campo Palabra en Inglés (not blank)
         redirect_to flashcards_review_path(:filter => flashcards_filter, :size => flashcards_size, :index => flashcards_index, :ranking => ranking, :save_location => false), alert: t('form.interacted_word.errors.word.blank') and return if (user_response.nil? || user_response.empty?)
         
         # Obtener InteractedWord y UserWord
         interacted_word = InteractedWord.find(interacted_word_id)
         user_word = UserWord.find(interacted_word.user_word_id)
         exclude = (exclude.nil? || exclude.empty?) ? interacted_word_id : exclude + ',' + interacted_word_id
         puts "EXCLUDE CONCAT: #{exclude}"
         
         # Mensaje de alerta y bandera de actualización
         alert_message = ''
         
         # Crear objeto especial SRInteractedWord para el Algoritmo SR
         sr_interacted_word = SRInteractedWord.new(
            interacted_word.id, # id
            interacted_word.repetitions, # repetitions
            interacted_word.encounters, # encounters
            interacted_word.record_repetition, # record_repetition
            InteractedWord.datetime_to_text(interacted_word.user_first_review), # user_first_review
            InteractedWord.datetime_to_text(interacted_word.user_last_review), # user_last_review
            InteractedWord.datetime_to_text(interacted_word.user_next_review), # user_next_review
            interacted_word.probability # probability
         )
         # Instanciar la clase del Algoritmo SR
         spaced_repetition = SpacedRepetition.new()
         
         if (user_response == user_word.word) # PALABRA CORRECTA
            puts "Palabra correcta"
            # Invocar el cálculo de la siguiente fecha de repaso de la palabra (CASO: palabra correcta)
            sr_interacted_word = spaced_repetition.calculate_next_review(sr_interacted_word, SR::DEFAULT_HOUR, SR::CORRECT_REVIEW)
            # Mensaje de respuesta correcta
            alert_message = "#{t('alert.flashcard.review.correct.body')} #{flashcards_index}: <b>#{user_word.word}</b>"
            ranking += 1
         else # PALABRA INCORRECTA
            puts "Palabra incorrecta"
            # Invocar el cálculo de la siguiente fecha de repaso de la palabra (CASO: palabra incorrecta)
            sr_interacted_word = spaced_repetition.calculate_next_review(sr_interacted_word, SR::DEFAULT_HOUR, SR::WRONG_REVIEW)
            # Mensaje de respuesta incorrecta
            alert_message = "#{t('alert.flashcard.review.incorrect.body')} #{flashcards_index}: <b>#{user_word.word}</b>"
         end
         
         # Actualizar base de datos con información del algoritmo SR
         is_update = interacted_word.update(repetitions:sr_interacted_word.repetitions, encounters:sr_interacted_word.encounters, record_repetition:sr_interacted_word.record_repetition, user_last_review:sr_interacted_word.user_last_review, user_next_review:sr_interacted_word.user_next_review, probability:sr_interacted_word.probability)
         # Incrementar indice de Tarjeta
         flashcards_index += 1
                  
         if (is_update)
            
            if (flashcards_index > flashcards_size) # Fue la última Tarjeta
               alert_message =  "#{alert_message}\n <hr> <b>#{t('alert.flashcard.ranking.head')}</b> #{ranking} #{t('alert.flashcard.ranking.body')} #{flashcards_size} #{t('alert.flashcard.ranking.tail')}"
               
               redirect_to return_back_or_default_path(flashcards_path), notice: alert_message and return
            else # Siguiente Tarjeta
               redirect_to flashcards_review_path(:filter => flashcards_filter, :size => flashcards_size, :index => flashcards_index, :ranking => ranking, :exclude => exclude, :save_location => false), notice: alert_message and return
            end
            
         else
            redirect_to return_back_or_default_path(flashcards_path), alert: t('alert.flashcard.error.update') and return
         end
         
   end
   
   def add_words_from_test()
      words = params[:words]
      
      @message = "¡Listo! Se han guardado en tu vocabulario de estudio las siguientes palabras desconocidas:"

      index = 1
      words.values.each do |value|
         if (value == '0')
            word          = t("modal.test.words.W#{index}.word")
            word_es       = t("modal.test.words.W#{index}.word_es")
            word_image    = t("modal.test.words.W#{index}.image")
            text_id       = t("modal.test.words.W#{index}.text_id")
            
            # Obtener lemma de la palabra
            lemma = UserWord.lemmatize(Const::LEMMATIZE_WORD, word)
            
            # Obtener oración con la palabra desde su texto
            sentence = t("modal.test.words.W#{index}.sentence")
            
            # UserWord buscar/obtener o crear
            user_word = UserWord.find_or_create_by(word:word, lemma:lemma)
            
            # InteractedWord existe o no?
            unless (InteractedWord.where(user_word_id:user_word.id, user_id:current_user.id, word_es:word_es).any?)
               # El InteractedWord consultado no existe, entonces crear uno nuevo
               
               # Crear objeto especial SRInteractedWord para el Algoritmo SR
               sr_interacted_word = SRInteractedWord.new(
                  0, # id
                  0, # repetitions
                  0, # encounters
                  0, # record_repetition
                  InteractedWord.datetime_to_text(DateTime.now()), # user_first_review
                  InteractedWord.datetime_to_text(DateTime.now()), # user_last_review
                  0, # user_next_review
                  0.0 # probability
               )
               # Instanciar la clase del Algoritmo SR
               spaced_repetition = SpacedRepetition.new()
               # Invocar el cálculo de la siguiente fecha de repaso de la palabra
               sr_interacted_word = spaced_repetition.calculate_next_review(sr_interacted_word, SR::DEFAULT_HOUR, SR::CORRECT_REVIEW)
               
               interacted_word = InteractedWord.create(
                  word_es:word_es,
                  sentence:sentence,
                  word_image:word_image,
                  repetitions:sr_interacted_word.repetitions,
                  encounters:sr_interacted_word.encounters,
                  record_repetition:sr_interacted_word.record_repetition,
                  user_first_review:sr_interacted_word.user_first_review,
                  user_last_review:sr_interacted_word.user_last_review,
                  user_next_review:sr_interacted_word.user_next_review,
                  probability:sr_interacted_word.probability,
                  user_word_id:user_word.id,
                  user_id:current_user.id,
                  text_id:text_id
               )
               
               if (interacted_word.valid?)
                  @message = @message + ' •' + word
               else
                  @message = @message + ' •' + word + '(error desconocido, no se ha guardado)'
                  puts "Error: no se ha podido guardar la palabra '#{word}/#{word_es}' para #{current_user.id}"
               end
            else
               # El InteractedWord consultado exite, entonces mandar mensaje de que ya existe
               puts "<b>#{word} (#{word_es})</b> ya existe en el vocabulario de #{current_user.id}."
            end
         end
         
         index += 1
      end
      
      session[:init_test] = false
      redirect_to home_path(), notice: @message and return
   end
   
   # Services (Google Cloud Platform)
   
   def translate_service()
      if (params[:authenticity_token] == session[:_csrf_token])
         @translation = UserWord.translate(params[:word])
      else
         puts "Error: token no válido (service_translate)"
      end
   end
   
   def images_service()
      if (params[:authenticity_token] == session[:_csrf_token])
         @images = UserWord.images(params[:word])
      else
         puts "Error: token no válido (service_images)"
      end
   end
   
   def text_to_speech_service()
      if (params[:authenticity_token] == session[:_csrf_token])
         # Base64.encode64()        - codifica línea por línea del binario (agrega salto de línea)
         # Base64.strict_encode64() - codifica todo el binario (crea un string continuo)
         @audio_content = Base64.strict_encode64( UserWord.text_to_speech(params[:word]) )
      else
         puts "Error: token no válido (service_text_to_speech)"
      end
   end
   
end