#encoding: UTF-8
# KHS: Controlador para registration (user)
class RegistrationsController < Devise::RegistrationsController
   
   # Insert new user
   
   def new()
      super
   end
   
   def create()
      super { |resource|
         resource.insert_gender_association()
         resource.insert_learning_style_association()
         resource.insert_cefr_association()
         resource.update_role(Const::STUDENT_ROLE)
      }
   end
   
   
   # Update user
   
   def edit()
      super      
   end
   
   def update()
      # Implementación de Devise con modificaciones
      self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
      prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)
      
      resource_updated = update_resource(resource, account_update_params)
      yield resource if block_given?
      
      if resource_updated
         self.resource.update_gender_association()
         self.resource.update_learning_style_association()
         self.resource.update_cefr_association()
         
         if ( ! self.resource.code_confirmation.blank?)
            
            if (self.resource.code_confirmation == resource.code && self.resource.role == Const::STUDENT_ROLE)
               self.resource.update_role(Const::TEACHER_ROLE)
            elsif (self.resource.code_confirmation.downcase == Const::STUDENT_ROLE && self.resource.role == Const::TEACHER_ROLE)
               self.resource.update_role(Const::STUDENT_ROLE)
            else
               # puts "CODIGO NO VALIDO"
               # self.resource.errors.add(:code_confirmation, :invalid, message: t('activerecord.errors.code_confirmation.incomplete'))
               # respond_with resource, location: edit_user_registration_path
               redirect_to home_path, alert: t('activerecord.errors.models.user.attributes.code_confirmation.incomplete')
               return
            end
         end
         
         # puts "CODIGO VACIO"
         set_flash_message_for_update(resource, prev_unconfirmed_email)
         bypass_sign_in resource, scope: resource_name if sign_in_after_change_password?
         respond_with resource, location: after_update_path_for(resource)
      else
         clean_up_passwords resource
         set_minimum_password_length
         respond_with resource
      end
      
   end
   
   
   # Delete user
   
   def destroy()
      begin
         resource.destroy_all_data() # Borrado de datos relacionados al usuario
      rescue StandardError => exception
         puts "ERROR LOG: no se han eliminado los datos correctamente, userID #{resource.id}."
         puts "LOG: #{exception}"
      else
         puts "LOG: se han eliminado los datos correctamente."
      ensure
         resource.destroy() # Borrado del usuario por Devise
         Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
         
         redirect_to new_user_session_path, notice: t('alert.account.deleted')
      end
   end
   
   
   protected
     
   
  def sign_up_params()
     params.require(:user).permit(:email, :first_name, :last_name, :birthdate, :gender_id, :learning_style_id, :cefr_profile_id, :password, :password_confirmation)
  end
  
  def account_update_params()
    params.require(:user).permit(:email, :first_name, :last_name, :birthdate, :gender_id, :learning_style_id, :cefr_profile_id, :password, :password_confirmation, :current_password, :code_confirmation)
  end
   
end 