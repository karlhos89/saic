class ApplicationController < ActionController::Base
   include Pagy::Backend # Pagy gem dependency
   
   # KHS: parámetros para create y update, adicionales a los que maneja Devise por default
   # before_action :configure_permitted_parameters, if: :devise_controller?
   before_action :load_menu_items, if: :authenticate_user!
   before_action :load_footer_buttons, except: [:configure_permitted_parameters]
   before_action :load_instructions, except: [:configure_permitted_parameters]
   before_action :load_participants, except: [:configure_permitted_parameters]
   
   MenuItem = Struct.new(:image, :rights_owner, :rights_url, :title, :description, :button, :symbol, :tooltip, :path, :method)
   
   Button      = Struct.new(:name, :type, :target)
   Instruction = Struct.new(:title, :body, :image)
   Participant = Struct.new(:name, :image, :thesis_work, :academic_degree, :investigation_line, :email)
   
   @menu = nil
   
   # Crear un mensaje de alerta si el Model de ActiveRecord tiene problemas para create o update
   def create_alert(model)
      alert = ""
      alert.concat("<b>")
      alert.concat( I18n.t("errors.messages.not_saved", count: model.errors.count, resource: model.class.model_name.human.downcase) )
      alert.concat("</b>")
      alert.concat("<br>")
      alert.concat("<ul>")
      
      model.errors.full_messages.each do |message|
         alert.concat("<li> El campo #{message} </li>")
      end
      
      alert.concat("</ul>")
      
      return alert # En formato HTML
   end
   
   # Redireccionar hacia una ruta guardada o una default
   def return_back()
      locations = session[:return_back]
      last_location = locations.last()
      
      locations.pop()
      session[:return_back] = locations
      
      redirect_to("#{last_location}?save_location=false" || params[:default])
      
   end
   
   # Limpiar la ruta almacenada previamente
   def clean_return_back()
      session[:return_back] = nil
   end
   
   # Detectar navegador
   def what_browser()
      user_agent = request.env['HTTP_USER_AGENT']
      
      if (user_agent =~ /Edg/)
         return Const::EDGE_BROWSER
      elsif (user_agent =~ /Firefox/)
         return Const::FIREFOX_BROWSER
      elsif (user_agent =~ /Safari/)
         return Const::SAFARI_BROWSER
      else
         return Const::OTHER_BROWSER
      end
   end
   
   
   protected
   
   
   # Guardar ruta desde donde se realiza una petición
   def save_location(save = true)
      # Si save no está especificado (nulo o vacío), por default se guarda location
      # De lo contrario, se comprueba que save == true para guardar location
      unless (save.nil? || save.empty?)
         return
      else
         if (save.to_s.downcase == 'false')
            return
         end
      end
      
      if (session[:return_back].nil?)
         locations = Array.new()
      else
         locations = session[:return_back]
      end
      
      # Elimina los parametros de la url
      url = request.referer.split('?')[0] unless (request.referer.nil?)
      
      locations.push(url) unless (request.referer.nil?)
      session[:return_back] = locations
      puts "LOCATIONS: #{session[:return_back]}"
      # puts "REFER: #{request.referer}"
         # = request.referer
         # ||= request.referer
         # request.fullpath
   end
   
   # Carga de elementos del menú principal
   
   def load_menu_items()
      @menu = Array.new()
      
      locations = Array.new()
      # Students and Teachers
      locations << {:locale => "readings", :image => "cards/readings.jpg", :path => readings_path, :method => :get}
      locations << {:locale => "flashcards", :image => "cards/flashcards.jpg", :path => flashcards_path, :method => :get}
      # Only Teachers
      locations << {:locale => "groups", :image => "cards/groups.jpg", :path => home_path, :method => :get } if (current_user.role == Const::TEACHER_ROLE)
      
      locations.each do |location|
         item = MenuItem.new()
         item.image        = location[:image]
         item.rights_owner = t("#{location[:locale]}.image.owner")
         item.rights_url   = t("#{location[:locale]}.image.url")
         item.title        = t("#{location[:locale]}.title")
         item.description  = t("#{location[:locale]}.description.#{current_user.role}")
         item.button       = t("button.menu.#{location[:locale]}.label")
         item.symbol       = t("button.menu.#{location[:locale]}.symbol")
         item.tooltip      = t("button.menu.#{location[:locale]}.tooltip")
         item.path         = location[:path]
         item.method       = location[:method]
         
         @menu.insert(-1, item)
      end
      
      return @menu
   end
   
   
   # Carga de componentes del footer
   
   def load_footer_buttons()
      @buttons = Array.new()
      # What's SAIC?
      @buttons << Button.new(t('button.whats_saic'),"modal","#openWhatsSaic")
      @buttons << Button.new("•","separator","n/a")
      # Instructions
      @buttons << Button.new(t('button.instructions'),"modal","#openInstructions")
      @buttons << Button.new("•","separator","n/a")
      # About Us
      @buttons << Button.new(t('button.about_us'),"modal","#openAboutUs")
      @buttons << Button.new("•","separator","n/a")
      # Contact Us
      @buttons << Button.new(t('button.contact_us'),"link","mailto:carlos.peralta18ca@cenidet.edu.mx")
      @buttons << Button.new("•","separator","n/a")
      # FB
      @buttons << Button.new(t('button.systems'),"link","https://www.facebook.com/Sistemas-Cognitivos-Sistemas-Distribuidos-111247010455136")
      @buttons << Button.new("|","separator","n/a")
      # Powered By
      @buttons << Button.new(t('powered_by'),"label","n/a")
      @buttons << Button.new(t('button.cenidet'),"link","http://www.cenidet.tecnm.mx/")
   end
   
   def load_instructions()
      @instructions = Array.new()
      @instructions << Instruction.new(t('modal.instructions.one.title'), t('modal.instructions.one.description'), "instructions/reading.png")
      @instructions << Instruction.new(t('modal.instructions.two.title'), t('modal.instructions.two.description'), "instructions/recommendation.png")
      @instructions << Instruction.new(t('modal.instructions.three.title'), t('modal.instructions.three.description'), "instructions/word_selection.png")
      @instructions << Instruction.new(t('modal.instructions.four.title'), t('modal.instructions.four.description'), "instructions/image_selection.png")
      @instructions << Instruction.new(t('modal.instructions.five.title'), t('modal.instructions.five.description'), "instructions/vocabulary.png")
      @instructions << Instruction.new(t('modal.instructions.six.title'), t('modal.instructions.six.description'), "instructions/flashcards.png")
      @instructions << Instruction.new(t('modal.instructions.seven.title'), t('modal.instructions.seven.description'), "instructions/spaced_repetition.png")
   end
   
   def load_participants()
      @participants = Array.new()
      @participants << Participant.new(t('modal.about_us.participants.carlos.name'), "about/ing_carlos.jpg", t('modal.about_us.participants.carlos.thesis_work'), t('modal.about_us.participants.carlos.academic_degree'), t('modal.about_us.participants.carlos.investigation_line'), t('modal.about_us.participants.carlos.email'))
      @participants << Participant.new(t('modal.about_us.participants.noe.name'), "about/dr_noe.jpg", t('modal.about_us.participants.noe.thesis_work'), t('modal.about_us.participants.noe.academic_degree'), t('modal.about_us.participants.noe.investigation_line'), t('modal.about_us.participants.noe.email'))
      @participants << Participant.new(t('modal.about_us.participants.juan.name'), "about/dr_juan.jpg", t('modal.about_us.participants.juan.thesis_work'), t('modal.about_us.participants.juan.academic_degree'), t('modal.about_us.participants.juan.investigation_line'), t('modal.about_us.participants.juan.email'))
      @participants << Participant.new(t('modal.about_us.participants.andrea.name'), "about/dra_andrea.jpg", t('modal.about_us.participants.andrea.thesis_work'), t('modal.about_us.participants.andrea.academic_degree'), t('modal.about_us.participants.andrea.investigation_line'), t('modal.about_us.participants.andrea.email'))
      @participants << Participant.new(t('modal.about_us.participants.maximo.name'), "about/dr_maximo.jpg", t('modal.about_us.participants.maximo.thesis_work'), t('modal.about_us.participants.maximo.academic_degree'), t('modal.about_us.participants.maximo.investigation_line'), t('modal.about_us.participants.maximo.email'))
   end
   
end
