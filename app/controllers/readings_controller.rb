#encoding: UTF-8
require_relative '../text_recommendation/text_recommendation'
require_relative '../spaced_repetition/spaced_repetition'
# KHS: Controlador para readings
class ReadingsController < ApplicationController
   before_action :authenticate_user!
   before_action :verify_associated_texts, only: [:index, :index_read, :index_unread, :index_recommended, :index_shared, :index_uploaded]
   before_action :clean_return_back, only: [:index, :index_read, :index_unread, :index_recommended, :index_shared, :index_uploaded]
   before_action :find, only: [:show, :edit, :update, :destroy]
   
   @text = nil # current_text
   
   
   # List All
   
   def index()
      list_by(Const::ALL_READINGS_FILTER)
   end
   
   def index_read()
      list_by(Const::READ_READINGS_FILTER)
   end
   
   def index_unread()
      list_by(Const::UNREAD_READINGS_FILTER)
   end
   
   def index_recommended()
      # Obtener textos recomendados y no leídos
      associated_texts = AssociatedText.where(user:current_user).where(recommendation: true).where(status: Const::UNREAD_STATUS)
      # Obtener palabra del usuario con el próximo repaso más antiguo/próximo
      interacted_words = InteractedWord.where(user:current_user).order(user_next_review: :asc).limit(1)
      interacted_word = interacted_words.first
      # Bandera que indica si ya se ha lanzado el Algoritmo TR en la sesión actual o no
      already_recommended = session[:already_recommended].nil? ? false : session[:already_recommended]
      
      # El Algoritmo de Recomendación de Textos (Algoritmo TR) se lanza sólo una vez por sesión si la cantidad de textos recomendados y no leídos es menor a TEXTS_QUANTITY_VALIDATION (1 texto) o si los dias hasta el siguiente repaso de una palabra del usuario es menor a DAYS_UNTIL_REVIEW_VALIDATION (7 días).
      
      if ((associated_texts.size() < TR::TEXTS_QUANTITY_VALIDATION || interacted_word.days_until_next_review() < TR::DAYS_UNTIL_REVIEW_VALIDATION) && (! already_recommended))
         # Almacenamiento en HASH de session de bandera para lanzar el algoritmo una vez por sesión
         session[:already_recommended] = TR::ALREADY_RECOMMENDED_VALIDATION
         # Crear objeto especial para el Algoritmo TR: User to TRUser
         tr_user = TRUser.new(
            current_user.id,
            current_user.email,
            current_user.first_name,
            current_user.last_name,
            current_user.birthdate,
            current_user.cefr_profiles.first.id,
            current_user.cefr_profiles.first.level,
            current_user.cefr_profiles.first.name,
            current_user.cefr_profiles.first.description,
            current_user.genders.first.id,
            current_user.genders.first.name,
            current_user.genders.first.description,
            current_user.learning_styles.first.id,
            current_user.learning_styles.first.name,
            current_user.learning_styles.first.description
         )
         # Instanciar la clase del Algoritmo TR
         text_recommendation = TextRecommendation.new()
         # Invocar la generación de recomendaciones de texto
         tr_texts = text_recommendation.generate_text_recommendations(tr_user)
         # Limpiar las recomendaciones de texto antiguas
         text_recommendation.update_text_recommendations(tr_texts, tr_user)
      else
         puts "El Algoritmo TR ya fue ejecutado en esta sesión."
      end
      
      list_by(Const::RECOMMENDED_READINGS_FILTER)
   end
   
   def index_shared()
      list_by(Const::SHARED_READINGS_FILTER)
   end
   
   def index_uploaded() # Uploaded / Owner
      list_by(Const::UPLOADED_READINGS_FILTER)
   end
   
   # New / create
   
   def new()
      save_location(params[:save_location])
      
      @text = Text.new()
      # Recuperar valores previos del formulario, si existen
      @text.title = params[:title]
      @text.body  = params[:body]
      @text.cefr_profile_id = params[:cefr_profile_id]
   end
   
   def create()
      text_params = params[:text]
      @text = nil
   
      title = text_params[:title] # Título de la lectura
      body  = text_params[:body] # Cuerpo de la lectura (texto / lectura)
      tokens = 0 # Número de palabras en el texto
      types = 0 # Número de palabras no repetidas en el texto
      lemmas = 0 # Número de lemas no repetidos en el texto
      vocabulary = Array.new() # Vocabulario (array de Expressions)
      cefr_profile_id = text_params[:cefr_profile_id] # ID del Perfil MCER
            
      # Generar vocabulario
      unless ((body.nil? || body.empty?) || (title.nil? || title.empty?) || (cefr_profile_id.nil? || cefr_profile_id.empty?))
         tokens = Text.count_tokens(body)
         vocabulary  = Text.create_vocabulary(body)
         
         unless (vocabulary.nil? || !vocabulary.length==0)
            types  = Text.count_types(vocabulary)
            lemmas = Text.count_lemmas(vocabulary)
         else
            redirect_to readings_new_path(title:title, body:body, cefr_profile_id:cefr_profile_id, save_location: false), alert: t('alert.freeling.invalid') and return
         end
      end
      
      # Crear Text
      @text = Text.create(title:title, body:body, tokens:tokens, types:types, lemmas:lemmas, cefr_profile_id:cefr_profile_id)
      
      if (@text.valid?)
         # Crear relación Text / ProfiledText / CefrProfile
         @text.insert_cefr_association()
         
         # Crear relación Text / AssociatedText / User
         @text.insert_text_association(current_user, true)
      
         # Vocabulario a base de datos
         vocabulary.each do |expression|
            # Crear TextWord
            text_word = nil
            
            unless (TextWord.where(word:expression.word).any?)
               text_word = TextWord.create(word:expression.word, lemma:expression.lemma)
            else
               text_word = TextWord.find_by(word:expression.word)
            end
            
            # Crear relación Text / AssociatedWord / TextWord
            @text.insert_word_association(text_word, false, expression.word_frequency, expression.lemma_frequency)
         end
         
         redirect_to readings_uploaded_path, notice: t('alert.reading.created') # ORDER BY create_at desc or id
      else
         redirect_to readings_new_path(title: title, body: body, cefr_profile_id: cefr_profile_id, save_location: false), alert: create_alert(@text)
      end
   end
   
   # Show / read
   
   def show()
      save_location(params[:save_location])
      
      @text.update_status(current_user, Const::READ_STATUS)
   end
   
   # Edit / update
   
   def edit()
      save_location(params[:save_location])
      
      # # Recuperar valores previos del formulario, si existen
      @text.title = (params[:title].nil? || params[:title].empty?) ? @text.title : params[:title]
      @text.body  = (params[:body].nil? || params[:body].empty?) ? @text.body : params[:body]
      @text.cefr_profile_id = (params[:cefr_profile_id].nil? || params[:cefr_profile_id].empty?) ? @text.cefr_profile_id : params[:cefr_profile_id]
      
      
   end
   
   def update()
      text_params = params[:text]
      
      title = text_params[:title] # Título de la lectura
      body  = text_params[:body] # Cuerpo de la lectura (texto / lectura)
      tokens = 0 # Número de palabras en el texto
      types = 0 # Número de palabras no repetidas en el texto
      lemmas = 0 # Número de lemas no repetidos en el texto
      vocabulary = Array.new() # Vocabulario (array de Expressions)
      cefr_profile_id = text_params[:cefr_profile_id] # ID del Perfil MCER
      
      # Generar vocabulario
      unless ((body.nil? || body.empty?) || (title.nil? || title.empty?) || (cefr_profile_id.nil? || cefr_profile_id.empty?))
         tokens = Text.count_tokens(body)
         vocabulary  = Text.create_vocabulary(body)
         
         unless (vocabulary.nil? || !vocabulary.length==0)
            types  = Text.count_types(vocabulary)
            lemmas = Text.count_lemmas(vocabulary)
         else
            redirect_to readings_edit_path(title: title, body: body, cefr_profile_id: cefr_profile_id, save_location: false), alert: t('alert.freeling.invalid') and return
         end
      end
      
      # Actualizar Text
      is_update = @text.update(title:title, body:body, tokens:tokens, types:types, lemmas:lemmas, cefr_profile_id:cefr_profile_id)
      
      if (is_update)
         # Actualizar relación Text / ProfiledText / CefrProfile
         @text.update_cefr_association()
         
         # Actualizar relación Text / AssociatedText / User
         # @text.insert_text_association(current_user, true)
         
         # Eliminar relaciones anteriores del vocabulario
         @text.delete_all_word_associations()
         
         # Vocabulario a base de datos
         vocabulary.each do |expression|
            # Crear TextWord
            text_word = nil
            
            unless (TextWord.where(word:expression.word).any?)
               text_word = TextWord.create(word:expression.word, lemma:expression.lemma)
            else
               text_word = TextWord.find_by(word:expression.word)
            end
            
            # Crear relación Text / AssociatedWord / TextWord
            @text.insert_word_association(text_word, false, expression.word_frequency, expression.lemma_frequency)
         end
         
         # @text indica el texto a visualizar
         # save_location parametro que indica si se almacena la URL para volver atrás
         redirect_to return_back_or_default_path(readings_uploaded_path, save_location: false), notice: t('alert.reading.updated')
      else
          # Si alguno de los campos está vacío, no se va actualizar y se deben mostrar los mensajes de error
         temp_text = Text.new()
         temp_text.title = title
         temp_text.body = body
         temp_text.cefr_profile_id = cefr_profile_id
         temp_text.valid?
         
         redirect_to readings_edit_path(title: title, body: body, cefr_profile_id: cefr_profile_id, save_location: false), alert:create_alert(temp_text)
      end
   end
   
   # Destroy / delete
   
   def destroy()
      begin
         # Eliminar relación con palabras (vocabulario)
         @text.delete_all_word_associations()
         
         # Eliminar relación con usuarios
         @text.delete_all_text_associations()
         
         # Eliminar relación con perfil MCER
         @text.delete_cefr_association()
         
         # Eliminar relación con interacted_words
         @text.delete_all_interacted_word_associations
         
         # Eliminar lectura
         @text.destroy()
      rescue StandardError => exception
         puts "ERROR LOG: no se ha eliminado el texto correctamente (readings_controller.rb)."
         puts "LOG #{exception}"
         redirect_to readings_uploaded_path(), alert: t('alert.reading.error.deleted')
      else
         puts "LOG: se han eliminado los datos correctamente."
         redirect_to readings_uploaded_path(), notice: t('alert.reading.deleted')
      end
   end
   
   # Others
   
   def find()
      # find() busca un elemento por ID, devuelve una coincidencia
      @text = current_user.texts.find(params[:id])
      @text.cefr_profile_id = @text.cefr_profiles.first.id
   end
   
   
   protected
   
   
   # Verifica que User tenga Texts asociados
   def verify_associated_texts()
      associated_texts = AssociatedText.where(user:current_user)
                                       .where("associated_texts.status = 'unread'")
      if (associated_texts.size() < 10)
         update_associated_texts()
      end
   end
   
   # Obtiene Texts con el mismo cefr_profile de User
   # desde la base de datos de la plataforma y se los asgina a User
   def update_associated_texts()
      # JOIN neasted single level: texts < profiled_texts < cefr_profile (tabla < tabla intermedia < tabla)
      texts = Text.joins(profiled_texts: :cefr_profile)
                  .select("texts.*")
                  .where("cefr_profiles.level = '#{current_user.cefr_profiles.first.level}'")
                  
      # Insertar asociaciones de textos      
      texts.each do |text|
         text.insert_text_association(current_user, false)
      end
      
   end
   
   # Obtiene textos mediante un filtro, los pagina (Pagy) y los muestra
   def list_by(filter)
      # clean_return_back() # Limpiar las URL guardadas con save_location()
      
      @filter = filter
      associated_texts = nil
      
      if (filter == Const::ALL_READINGS_FILTER)
         associated_texts = AssociatedText.where(user:current_user)
      elsif (filter == Const::RECOMMENDED_READINGS_FILTER)
         associated_texts = AssociatedText.where(user:current_user).where("associated_texts.recommendation = true")
      elsif (filter == Const::SHARED_READINGS_FILTER)
         associated_texts = AssociatedText.where(user:current_user).where("associated_texts.share = true")
      elsif (filter == Const::UPLOADED_READINGS_FILTER) # Uploaded / Owner
         associated_texts = AssociatedText.where(user:current_user).where("associated_texts.owner = true").order(created_at: :desc)
      else
         associated_texts = AssociatedText.where(user:current_user).where("associated_texts.status = '#{filter}'")
      end
      
      @pagy, @associated_texts = pagy(associated_texts, items:10) # Paginación de textos
   end
   
   # Filtro de params
   # def readings_new_params()
   #    params.require(:text).permit(:title, :body, :cefr_profile_id)
   # end
   
end