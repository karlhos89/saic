#encoding: UTF-8
require_relative '../spaced_repetition/spaced_repetition'
# KHS: Controlador para flashcards
class FlashcardsController < ApplicationController
   before_action :authenticate_user!
   before_action :clean_return_back, only: [:index, :index_next, :index_expired, :index_standby, :index_challenge]
   
   # List All
   
   def index()
      interacted_words = find_flashcards_by(Const::ALL_FLASHCARDS_FILTER)
      @pagy, @interacted_words = pagy(interacted_words, items:10) # Paginación de textos
   end
   
   def index_next()
      interacted_words = find_flashcards_by(Const::NEXT_FLASHCARDS_FILTER)
      @pagy, @interacted_words = pagy(interacted_words, items:10) # Paginación de textos
   end
   
   def index_expired()
      interacted_words = find_flashcards_by(Const::EXPIRED_FLASHCARDS_FILTER)
      @pagy, @interacted_words = pagy(interacted_words, items:10) # Paginación de textos
   end
   
   def index_standby()
      interacted_words = find_flashcards_by(Const::STANDBY_FLASHCARDS_FILTER)
      @pagy, @interacted_words = pagy(interacted_words, items:10) # Paginación de textos
   end
   
   def index_challenge()
      interacted_words = find_flashcards_by(Const::CHALLENGE_FLASHCARDS_FILTER)
      @pagy, @interacted_words = pagy(interacted_words, items:10) # Paginación de textos
   end
   
   # Repaso de vocabulario
   
   def review()
      save_location(params[:save_location])
      
      # Flashcards: filter, size, index, ranking
      filter   = (params[:filter].nil? || params[:filter].empty?) ? 'none' : params[:filter]
      @size    = (params[:size].nil? || params[:size].empty?) ? 0 : params[:size].to_i
      @index   = (params[:index].nil? || params[:index].empty?) ? 1 : params[:index].to_i
      @ranking = (params[:ranking].nil? || params[:ranking].empty?) ? 0 : params[:ranking].to_i # Tarjetas correctas
      @exclude = (params[:exclude].nil? || params[:exclude].empty?) ? '' : params[:exclude].to_s #IDs a excluir
            
      @interacted_words = find_flashcards_by(filter, @size, @exclude.split(','))
      @current_interacted_word = (@interacted_words.nil? || @interacted_words.empty?) ? nil : @interacted_words.first
   end
   
   
   protected
   
   
   def find_flashcards_by(filter, size=-1, exclude=[])
      @filter = filter
      interacted_words = nil
      
      if (filter == Const::ALL_FLASHCARDS_FILTER)
         interacted_words = find_all_flashcards(size, exclude)
      elsif (filter == Const::NEXT_FLASHCARDS_FILTER)
         interacted_words = find_next_flashcards(size, exclude)
      elsif (filter == Const::EXPIRED_FLASHCARDS_FILTER)
         interacted_words = find_expired_flashcards(size, exclude)
      elsif (filter == Const::STANDBY_FLASHCARDS_FILTER)
         interacted_words = find_standby_flashcards(size, exclude)
      elsif (filter == Const::CHALLENGE_FLASHCARDS_FILTER)
         interacted_words = find_challenge_flashcards(size, exclude)
      end
      
      return interacted_words
   end
   
   # Find methods
   
   def find_all_flashcards(size, exclude)
      # Petición a DBMS para listas
      if (size <= 0 && exclude.size() == 0)
         return InteractedWord.where(user:current_user).order(user_next_review: :asc)
      end
      
      # Petición a DBMS para conjuntos con limite de 10 o 5 tarjetas y excluyendo IDs
      return InteractedWord.where(user:current_user).order(user_next_review: :asc).limit(size).where.not(id:exclude)
   end
   
   def find_next_flashcards(size, exclude)
      # Periodo de repaso de start_datetime a final_datetime (la diferencia es de DAYS_RANGE)
      start_datetime = InteractedWord.datetime_to_text(DateTime.now())
      final_datetime = InteractedWord.datetime_to_text(DateTime.now() + Const::DAYS_RANGE)
      
      # Petición a DBMS para listas
      if (size <= 0 && exclude.size() == 0)
         return InteractedWord.where(user:current_user).where("interacted_words.user_next_review >= '#{start_datetime}' AND interacted_words.user_next_review <= '#{final_datetime}'").order(user_next_review: :asc)
      end
      
      # Petición a DBMS para conjuntos con limite de 10 o 5 tarjetas y excluyendo IDs
      return InteractedWord.where(user:current_user).where("interacted_words.user_next_review >= '#{start_datetime}' AND interacted_words.user_next_review <= '#{final_datetime}'").order(user_next_review: :asc).limit(size).where.not(id:exclude)
   end
   
   def find_expired_flashcards(size, exclude)
      current_datetime = InteractedWord.datetime_to_text(DateTime.now())
      
      # Petición a DBMS para listas
      if (size <= 0 && exclude.size() == 0)
         return InteractedWord.where(user:current_user).where("interacted_words.user_next_review <= '#{current_datetime}'").order(user_next_review: :asc)
      end
      
      # Petición a DBMS para conjuntos con limite de 10 o 5 tarjetas y excluyendo IDs
      return InteractedWord.where(user:current_user).where("interacted_words.user_next_review <= '#{current_datetime}'").order(user_next_review: :asc).limit(size).where.not(id:exclude)
   end
   
   def find_standby_flashcards(size, exclude)
      range_datetime = InteractedWord.datetime_to_text(DateTime.now() + Const::DAYS_RANGE)
      
      # Petición a DBMS para listas
      if (size <= 0 && exclude.size() == 0)
         return InteractedWord.where(user:current_user).where("interacted_words.user_next_review >= '#{range_datetime}'").order(user_next_review: :asc)
      end
      
      # Petición a DBMS para conjuntos con limite de 10 o 5 tarjetas y excluyendo IDs
      return InteractedWord.where(user:current_user).where("interacted_words.user_next_review >= '#{range_datetime}'").order(user_next_review: :asc).limit(size).where.not(id:exclude)
   end
   
   def find_challenge_flashcards(size, exclude)
      interacted_words = InteractedWord.where(user:current_user).order(user_next_review: :asc)
      ids_array = Array.new()
      
      interacted_words.each do |interacted_word|
         if (interacted_word.difficulty() == Const::HIGH_DIFFICULTY)
            ids_array << interacted_word.id
         end
      end
      
      # Petición a DBMS para listas
      if (size <= 0 && exclude.size() == 0)
         return InteractedWord.where(id:ids_array).order(user_next_review: :asc)
      end
      
      # Petición a DBMS para conjuntos con limite de 10 o 5 tarjetas y excluyendo IDs
      return InteractedWord.where(id:ids_array).order(user_next_review: :asc).limit(size).where.not(id:exclude)
   end
   
end
