#encoding: UTF-8
# KHS: Controlador para errors
class ErrorsController < ApplicationController
   
   def file_not_found() # Error 404
      puts 'file_not_found()'
      # render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false # Default 404 page
   end
   
   def unprocessable() # Error 422
      puts 'unprocessable()'
   end
   
   def internal_server_error() # Error 500
      puts 'internal_server_error()'
   end
   
end