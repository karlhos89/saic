#!/bin/sh

# Eliminar variable de Google Translate
unset GOOGLE_APPLICATION_CREDENTIALS
echo "Deleting GOOGLE_APPLICATION_CREDENTIALS..."
# echo $GOOGLE_APPLICATION_CREDENTIALS


# Obtener PID del servidor Freeling
fpid=$(pgrep -u carlosquintero analyze) # carlosquintero cambiarlo por el usuario en el servidor
echo "Freeling Server PID: $fpid"

# Matar proceso del servidor Freeling mediante su PID
echo "Freeling Server: kill signal"
analyze stop $fpid


# Obtener PID del servidor Rails
filename="tmp/pids/server.pid" # Ruta
rpid=$(cat $filename) # Obtener PID y asignar a variable
echo "Rails Server PID: $rpid"

# Matar proceso del servidor Rails mediante su PID
echo "Rails Server: kill signal"
kill -2 $rpid
kill -9 $rpid