#!/bin/sh

# Exportar variable de Google Translate
echo "Exporting GOOGLE_APPLICATION_CREDENTIALS..."
export GOOGLE_APPLICATION_CREDENTIALS="config/google_cloud_translate/SAIC-c0aa60f3c448.json"
echo $GOOGLE_APPLICATION_CREDENTIALS


# Ejecutar servidor Freeling en segundo plano
echo "Launching Freeling server..."
analyze -f config/freeling/en.cfg --server --port 50005 &


# Ejecutar servidor Rails en segundo plano
echo "Launching Rails server..."
# rails server -d
rails server