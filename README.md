# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

   Ruby 2.7.2
   Rails 6.0.2.1
   See Gemgile for more information

* System dependencies
   
   See Gemgile for more information

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

   Freeling (server mode)
   Google Cloud Services (Translate and Custom Search)
   Run credentials and services information in initSAIC.sh file and stop credentials and services information in stopSAIC.sh file.

* Deployment instructions

   See https://gorails.com/deploy/ubuntu/20.04#next-steps or deploy folder in this project
   

* ...
