# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_21_213004) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answered_questions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "question_id", null: false
    t.bigint "answer_id", null: false
    t.boolean "calification", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["answer_id"], name: "index_answered_questions_on_answer_id"
    t.index ["question_id"], name: "index_answered_questions_on_question_id"
    t.index ["user_id"], name: "index_answered_questions_on_user_id"
  end

  create_table "answers", force: :cascade do |t|
    t.string "text", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "associated_answers", force: :cascade do |t|
    t.bigint "question_id", null: false
    t.bigint "answer_id", null: false
    t.boolean "is_correct", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["answer_id"], name: "index_associated_answers_on_answer_id"
    t.index ["question_id"], name: "index_associated_answers_on_question_id"
  end

  create_table "associated_genders", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "gender_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["gender_id"], name: "index_associated_genders_on_gender_id"
    t.index ["user_id"], name: "index_associated_genders_on_user_id"
  end

  create_table "associated_learning_styles", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "learning_style_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["learning_style_id"], name: "index_associated_learning_styles_on_learning_style_id"
    t.index ["user_id"], name: "index_associated_learning_styles_on_user_id"
  end

  create_table "associated_questions", force: :cascade do |t|
    t.bigint "text_id", null: false
    t.bigint "question_id", null: false
    t.string "difficulty", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "index_associated_questions_on_question_id"
    t.index ["text_id"], name: "index_associated_questions_on_text_id"
  end

  create_table "associated_texts", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "text_id", null: false
    t.string "status", null: false
    t.boolean "recommendation", null: false
    t.float "recommendation_quality"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "owner", default: false, null: false
    t.boolean "share", default: false, null: false
    t.string "share_by"
    t.index ["text_id"], name: "index_associated_texts_on_text_id"
    t.index ["user_id"], name: "index_associated_texts_on_user_id"
  end

  create_table "associated_words", force: :cascade do |t|
    t.bigint "text_id", null: false
    t.bigint "text_word_id", null: false
    t.integer "word_frequency", null: false
    t.boolean "top_word", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "lemma_frequency", null: false
    t.index ["text_id"], name: "index_associated_words_on_text_id"
    t.index ["text_word_id"], name: "index_associated_words_on_text_word_id"
  end

  create_table "cefr_profiles", force: :cascade do |t|
    t.string "level", null: false
    t.string "name", null: false
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["level"], name: "index_cefr_profiles_on_level", unique: true
  end

  create_table "genders", force: :cascade do |t|
    t.string "name", null: false
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_genders_on_name", unique: true
  end

  create_table "grouped_users", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "group_id", null: false
    t.boolean "notify", default: true, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["group_id"], name: "index_grouped_users_on_group_id"
    t.index ["user_id"], name: "index_grouped_users_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_groups_on_name", unique: true
  end

  create_table "interacted_words", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "text_id"
    t.bigint "user_word_id", null: false
    t.string "word_es", null: false
    t.text "sentence"
    t.text "word_image"
    t.integer "repetitions", null: false
    t.integer "encounters", null: false
    t.integer "record_repetition", null: false
    t.datetime "user_first_review", null: false
    t.datetime "user_last_review", null: false
    t.datetime "user_next_review", null: false
    t.float "probability", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["text_id"], name: "index_interacted_words_on_text_id"
    t.index ["user_id"], name: "index_interacted_words_on_user_id"
    t.index ["user_word_id"], name: "index_interacted_words_on_user_word_id"
  end

  create_table "learning_styles", force: :cascade do |t|
    t.string "name", null: false
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_learning_styles_on_name", unique: true
  end

  create_table "profiled_texts", force: :cascade do |t|
    t.bigint "text_id", null: false
    t.bigint "cefr_profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cefr_profile_id"], name: "index_profiled_texts_on_cefr_profile_id"
    t.index ["text_id"], name: "index_profiled_texts_on_text_id"
  end

  create_table "profiled_users", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "cefr_profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cefr_profile_id"], name: "index_profiled_users_on_cefr_profile_id"
    t.index ["user_id"], name: "index_profiled_users_on_user_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string "text", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "text_words", force: :cascade do |t|
    t.string "word", null: false
    t.string "lemma", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["word"], name: "index_text_words_on_word", unique: true
  end

  create_table "texts", force: :cascade do |t|
    t.string "title", null: false
    t.text "body", null: false
    t.integer "tokens", null: false
    t.integer "types", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "lemmas", null: false
  end

  create_table "user_words", force: :cascade do |t|
    t.string "word", null: false
    t.string "lemma", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["word"], name: "index_user_words_on_word", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.date "birthdate", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "role", default: "student", null: false
    t.string "code", default: "SAIC", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "answered_questions", "answers"
  add_foreign_key "answered_questions", "questions"
  add_foreign_key "answered_questions", "users"
  add_foreign_key "associated_answers", "answers"
  add_foreign_key "associated_answers", "questions"
  add_foreign_key "associated_genders", "genders"
  add_foreign_key "associated_genders", "users"
  add_foreign_key "associated_learning_styles", "learning_styles"
  add_foreign_key "associated_learning_styles", "users"
  add_foreign_key "associated_questions", "questions"
  add_foreign_key "associated_questions", "texts"
  add_foreign_key "associated_texts", "texts"
  add_foreign_key "associated_texts", "users"
  add_foreign_key "associated_words", "text_words"
  add_foreign_key "associated_words", "texts"
  add_foreign_key "grouped_users", "groups"
  add_foreign_key "grouped_users", "users"
  add_foreign_key "interacted_words", "texts"
  add_foreign_key "interacted_words", "user_words"
  add_foreign_key "interacted_words", "users"
  add_foreign_key "profiled_texts", "cefr_profiles"
  add_foreign_key "profiled_texts", "texts"
  add_foreign_key "profiled_users", "cefr_profiles"
  add_foreign_key "profiled_users", "users"
end
