class CreateGroupedUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :grouped_users do |t|
      t.references :user, null: false, foreign_key: true
      t.references :group, null: false, foreign_key: true
      t.boolean :notify, null: false, default: true

      t.timestamps
    end
  end
end

# rails generate model GroupedUser user:references group:references 