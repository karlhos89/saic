class CreateProfiledTexts < ActiveRecord::Migration[6.0]
   def change
      create_table :profiled_texts do |t|
         t.references :text, null: false, foreign_key: true
         t.references :cefr_profile, null: false, foreign_key: true

         t.timestamps
      end    
   end
end
