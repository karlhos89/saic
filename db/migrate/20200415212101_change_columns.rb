class ChangeColumns < ActiveRecord::Migration[6.0]
   def change
      change_column_null :user_words, :lemma, false
      
      change_column_null :learning_styles, :description, true
      
      change_column_null :genders, :description, true
      
      change_column_null :cefr_profiles, :description, null: true
      
      change_column_null :texts, :tokens, false
      change_column_null :texts, :types, false
      change_column_null :texts, :lemmas, false
      
      change_column_null :text_words, :lemma, false
      
      change_column_null :associated_words, :lemma_frequency, false
   end
end
