class CreateCefrProfiles < ActiveRecord::Migration[6.0]
   def change
      create_table :cefr_profiles do |t|
         t.string :level, null:false
         t.string :name, null: true
         t.string :description, null: true

         t.timestamps
      end
      
      add_index :cefr_profiles, :level, unique: true
   end
end
