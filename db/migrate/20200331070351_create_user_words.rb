class CreateUserWords < ActiveRecord::Migration[6.0]
   def change
      create_table :user_words do |t|
         t.string :word, null: false
         t.string :lemma, null: true

         t.timestamps
      end
      
      add_index :user_words, :word, unique: true
   end
end

# rails generate model UserWord word:string
# rails db:migrate
# rails db:rollback
# rails db:rollback STEP=8