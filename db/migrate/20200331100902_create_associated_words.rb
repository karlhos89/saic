class CreateAssociatedWords < ActiveRecord::Migration[6.0]
  def change
    create_table :associated_words do |t|
      t.references :text, null: false, foreign_key: true
      t.references :text_word, null: false, foreign_key: true
      t.integer :frequency, null: false
      t.boolean :top_word, null: false
      
      t.timestamps
    end
  end
end

# rails generate model AssociatedWord text:references text_word:references