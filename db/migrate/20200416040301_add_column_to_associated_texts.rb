class AddColumnToAssociatedTexts < ActiveRecord::Migration[6.0]
  def change
    add_column :associated_texts, :owner, :boolean, null: false, default: false
    add_column :associated_texts, :share, :boolean, null: false, default: false
    add_column :associated_texts, :share_by, :string, null: true, default: nil
  end
end

# rails generate migration add_column_to_associated_texts owner:boolean