class CreateProfiledUsers < ActiveRecord::Migration[6.0]
   def change
      create_table :profiled_users do |t|
         t.references :user, null: false, foreign_key: true
         t.references :cefr_profile, null: false, foreign_key: true

         t.timestamps
      end    
   end
end
