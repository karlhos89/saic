class CreateAssociatedLearningStyles < ActiveRecord::Migration[6.0]
   def change
      create_table :associated_learning_styles do |t|
         t.references :user, null: false, foreign_key: true
         t.references :learning_style, null: false, foreign_key: true

         t.timestamps
      end      
   end
end

# rails generate model AssociatedLearningStyle User:references LearningStyle:references