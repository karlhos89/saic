class CreateAssociatedAnswers < ActiveRecord::Migration[6.0]
   def change
      create_table :associated_answers do |t|
         t.references :question, null: false, foreign_key: true
         t.references :answer, null: false, foreign_key: true
         t.boolean :is_correct, null: false

         t.timestamps
      end    
   end
end
