class CreateLearningStyles < ActiveRecord::Migration[6.0]
   def change
      create_table :learning_styles do |t|
         t.string :name, null:false
         t.string :description, null: true

         t.timestamps
      end
      
      add_index :learning_styles, :name, unique: true
   end
end
