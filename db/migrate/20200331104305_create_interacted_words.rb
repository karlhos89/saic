class CreateInteractedWords < ActiveRecord::Migration[6.0]
  def change
    create_table :interacted_words do |t|
      t.references :user, null: false, foreign_key: true
      t.references :text, null: true, foreign_key: true
      t.references :user_word, null: false, foreign_key: true
      t.string :word_es, null: false
      t.text :sentence, null: true
      t.binary :word_image, null: true
      t.integer :repetitions, null: false
      t.integer :encounters, null: false
      t.integer :record_repetition, null: false
      t.datetime :user_first_review, null: false
      t.datetime :user_last_review, null: false
      t.datetime :user_next_review, null: false
      t.float :probability, null: false
      
      t.timestamps
    end
  end
end
