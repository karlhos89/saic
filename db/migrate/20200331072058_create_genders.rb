class CreateGenders < ActiveRecord::Migration[6.0]
   def change
      create_table :genders do |t|
         t.string :name, null:false
         t.string :description, null: true

         t.timestamps
      end
      
      add_index :genders, :name, unique: true
   end
end
