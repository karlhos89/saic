class AddColumnToUsers < ActiveRecord::Migration[6.0]
   def change
      add_column :users, :role, :string, null: false, default: "student"
      add_column :users, :code, :string, null: false, default: "SAIC"
   end
end

# rails generate migration add_column_to_users teacher:boolean