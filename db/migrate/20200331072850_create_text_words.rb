class CreateTextWords < ActiveRecord::Migration[6.0]
   def change
      create_table :text_words do |t|
         t.string :word, null:false
         t.string :lemma, null: true

         t.timestamps
      end
      
      add_index :text_words, :word, unique: true
   end
end
