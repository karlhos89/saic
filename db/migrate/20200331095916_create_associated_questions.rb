class CreateAssociatedQuestions < ActiveRecord::Migration[6.0]
   def change
      create_table :associated_questions do |t|
         t.references :text, null: false, foreign_key: true
         t.references :question, null: false, foreign_key: true
         t.string :difficulty, null: false
         
         t.timestamps
      end
   end
end
