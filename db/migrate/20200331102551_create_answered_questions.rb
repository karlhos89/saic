class CreateAnsweredQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :answered_questions do |t|
      t.references :user, null: false, foreign_key: true
      t.references :question, null: false, foreign_key: true
      t.references :answer, null: false, foreign_key: true
      t.boolean :calification, null: false
      
      t.timestamps
    end
  end
end

# rails generate model AnsweredQuestion user:references question:references answer:references