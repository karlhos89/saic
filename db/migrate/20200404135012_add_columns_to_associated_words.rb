class AddColumnsToAssociatedWords < ActiveRecord::Migration[6.0]
   def change
      rename_column :associated_words, :frequency, :word_frequency
      add_column :associated_words, :lemma_frequency, :integer
   end
end

# rails generate migration add_columns_to_associated_words word_frequency:integer lemma_frequency:integer