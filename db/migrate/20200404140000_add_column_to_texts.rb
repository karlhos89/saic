class AddColumnToTexts < ActiveRecord::Migration[6.0]
   def change
      add_column :texts, :lemmas, :integer
   end
end

# rails generate migration add_column_to_texts lemmas:integer