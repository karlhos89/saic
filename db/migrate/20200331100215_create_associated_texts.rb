class CreateAssociatedTexts < ActiveRecord::Migration[6.0]
  def change
    create_table :associated_texts do |t|
      t.references :user, null: false, foreign_key: true
      t.references :text, null: false, foreign_key: true
      t.string :status, null: false
      t.boolean :recommendation, null: false
      t.float :recommendation_quality, null: true
      
      t.timestamps
    end
  end
end

# rails generate model AssociatedText user:references text:references