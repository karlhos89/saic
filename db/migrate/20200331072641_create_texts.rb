class CreateTexts < ActiveRecord::Migration[6.0]
   def change
      create_table :texts do |t|
         t.string :title, null: false
         t.text :body, null: false
         t.integer :tokens, null: true
         t.integer :types, null: true

         t.timestamps
      end
   end
end
