class ChangeWordImageColumnInInteractedWords < ActiveRecord::Migration[6.0]
   def up
      change_column :interacted_words, :word_image, :text
   end
  
   def down
      change_column :interacted_words, :word_image, :binary
   end
end
# rails generate migration change_word_image_column_in_interacted_words
# rails db:migrate
# rails db:rollback
# rails db:rollback STEP=8